part of siriussign_sdk.ipfs;

class DownloadParameter {
  String transactionHash;
  PrivacyStrategy privacyStrategy;
  bool validateDigest;
  String accountPrivateKey;

  DownloadParameter(
    this.transactionHash,
    this.privacyStrategy,
    this.validateDigest,
    this.accountPrivateKey,
  );
}
