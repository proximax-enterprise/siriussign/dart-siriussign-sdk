part of siriussign_sdk;

/// SiriusDocument is a model of signing document used in SiriusSign.
/// It contains file and its notarization transaction infomation.
class SiriusSignDocumentInfo {
  bool isCompleted;
  bool isVerified;

  /// Document ID
  String id;

  /// File name
  String fileName;

  /// File type
  String fileType;

  /// File hash
  String fileHash;

  /// File upload and sign date
  DateTime uploadDate;

  /// File owner address, who sign this document first
  SignerInfo owner;

  /// Document cosigners
  List<SignerInfo> cosigners;

  /// Document Account that file belongs to
  PublicAccount documentAccount;

  /// Upload transaction hash
  String uploadTxHash;

  /// File is encrypt
  int encryptType;

  /// Document verifiers
  List<SignerInfo> verifiers;

  SiriusSignDocumentInfo._(
      this.id,
      this.fileName,
      this.fileType,
      this.fileHash,
      this.uploadDate,
      this.owner,
      this.cosigners,
      this.documentAccount,
      this.uploadTxHash,
      this.encryptType,
      this.verifiers);

  /// Create a object that contains Sirius Sign Document Info
  static create(
      String id,
      String fileName,
      String fileType,
      String fileHash,
      DateTime uploadDate,
      SignerInfo owner,
      List<SignerInfo> cosigners,
      PublicAccount documentAccount,
      String uploadTxHash,
      int encryptType,
      List<SignerInfo> verifiers) {
    SiriusSignDocumentInfo doc = new SiriusSignDocumentInfo._(
        id,
        fileName,
        fileType,
        fileHash,
        uploadDate,
        owner,
        cosigners,
        documentAccount,
        uploadTxHash,
        encryptType,
        verifiers);
    doc.checkIsCompleted();
    doc.checkIsVerified();
    return doc;
  }

  /// Create a object that contains Sirius Sign Document Info
  static SiriusSignDocumentInfo init(
    String fileName,
    String fileType,
    String fileHash,
  ) {
    SignerInfo initOwner = SignerInfo(null, null, false, null, null, null);
    SiriusSignDocumentInfo doc = new SiriusSignDocumentInfo._(
        null,
        fileName,
        fileType,
        fileHash,
        null,
        initOwner,
        [],
        null,
        null,
        EncryptType.PLAIN,
        []);
    doc.checkIsCompleted();
    return doc;
  }

  toJson() {
    final json = {
      "isCompleted": isCompleted,
      "isVerified": isVerified,
      "id": id,
      "fileName": fileName,
      "fileType": fileType,
      "fileHash": fileHash,
      "uploadDate": uploadDate,
      "owner": owner.toJson(),
      "cosigners": cosigners.map((e) => e.toJson()).toList(),
      "documentAccount": documentAccount.toJson(),
      "uploadTxHash": uploadTxHash,
      "encryptType": encryptType,
      "verifiers": verifiers.map((e) => e.toJson()).toList(),
    };
    return json;
  }

  List<String> getCosignerAddresses() {
    List<String> cosignerAddresses =
        this.cosigners.map((cosigner) => cosigner.address).toList();
    return cosignerAddresses;
  }

  List<String> getSignedCosignerAddresses() {
    List<String> signedCosignerAddresses = this
        .cosigners
        .where((cosigner) => cosigner.isSigned)
        .map((cosigner) => cosigner.address)
        .toList();
    return signedCosignerAddresses;
  }

  List<String> getVerifierAddresses() {
    List<String> verifierAddresses =
        this.verifiers.map((verifier) => verifier.address).toList();
    return verifierAddresses;
  }

  List<String> getVerifiedVerifierAddresses() {
    List<String> verifiedVerifierAddresses = this
        .verifiers
        .where((verifier) => verifier.isSigned)
        .map((verifier) => verifier.address)
        .toList();
    return verifiedVerifierAddresses;
  }

  bool checkIsCompleted() {
    List<String> cosigners = this.getCosignerAddresses();
    List<String> cosignatures = this.getSignedCosignerAddresses();
    this.isCompleted = (cosigners.length == cosignatures.length) &&
        (this.owner?.isSigned ?? false);
    return this.isCompleted;
  }

  bool checkIsVerified() {
    List<String> verifiers = this.getVerifierAddresses();
    List<String> verifieds = this.getVerifiedVerifierAddresses();
    return verifiers.length == verifieds.length;
  }

  SiriusSignDocumentInfo addSignatureUploadTxHash(String address, String hash) {
    bool isOwner = this.owner.address == address;
    if (isOwner) {
      this.owner.signatureUploadTxHash = hash;
    } else {
      List<String> cosigners = this.getCosignerAddresses();
      int selectedIndex = cosigners.indexOf(address);
      if (selectedIndex < 0)
        throw ('D01: The address is not a signer of this document');
      this.cosigners[selectedIndex].signatureUploadTxHash = hash;
    }
    return this;
  }

  SiriusSignDocumentInfo addSignTxHash(
      String address, String hash, DateTime date) {
    bool isOwner = this.owner.address == address;
    if (isOwner) {
      this.owner.signTxHash = hash;
      this.owner.signDate = date;
      this.owner.isSigned = true;
    } else {
      List<String> cosigners = this.getCosignerAddresses();
      int selectedIndex = cosigners.indexOf(address);
      if (selectedIndex < 0)
        throw ('D01: The address is not a signer of this document');
      this.cosigners[selectedIndex].signTxHash = hash;
      this.cosigners[selectedIndex].signDate = date;
      this.cosigners[selectedIndex].isSigned = true;
    }
    return this;
  }

  bool checkIsSigner(String address) {
    bool isSigner = [this.owner, ...this.cosigners]
        .map((signer) => signer.address)
        .toList()
        .contains(address);
    return isSigner;
  }

  bool checkIsVerifier(String address) {
    bool isVerifier =
        this.verifiers.map((verifier) => verifier.address).contains(address);
    return isVerifier;
  }
}
