part of siriussign_sdk;

class RawAccountInfo {
  String publicKey;
  Address address;

  RawAccountInfo(this.publicKey, this.address);
}

class DocumentInfoService extends TransactionFetch {
  String appID;
  bool _useStrict = false;

  DocumentInfoService(String apiNode, int networkType, [int queryPageSize = 10])
      : super(apiNode, networkType, queryPageSize);

  /// Set functions to filter Sirius Sign transaction message with given appID
  /// @param appID
  void setStrictFilter(String appID) {
    this.appID = appID;
    this._useStrict = true;
  }

  /// Clear strict filter
  void clearStrictFilter() {
    this._useStrict = false;
  }

  /// Check if app ID in message is the same to given app ID if strict filter is set
  /// @param appID
  @protected
  bool checkStrict(String appID) {
    return (this._useStrict && appID == this.appID) || (!this._useStrict);
  }

  /// Parse payload of transaction to Sirius Sign message
  /// @param payload
  SiriusSignMessage parseSiriusSignMessage(String payload) {
    const SiriusSignMessage errorResult = null;
    try {
      int type = SiriusSignMessage.getType(payload);
      switch (type) {
        case SiriusSignMessageType.SIGN:
          {
            return SiriusSignDocumentSigningMessage.createFromPayload(payload);
          }
        case SiriusSignMessageType.SIGN_NOTIFY:
          {
            return SiriusSignCosignerNotifyingMessage.createFromPayload(
                payload);
          }
        case SiriusSignMessageType.VERIFY:
          {
            return SiriusSignDocumentVerifyingMessage.createFromPayload(
                payload);
          }
        case SiriusSignMessageType.VERIFY_NOTIFY:
          {
            return SiriusSignVerifierNotifyingMessage.createFromPayload(
                payload);
          }
        default:
          return errorResult;
      }
    } catch (err) {
      return errorResult;
    }
  }

  /// Filter Document Signing Notifications from an array of Aggregate Commplete Transactions
  /// @param aggCompleteTxs
  @protected
  List<AggregateTransaction> filterDocumentSigningNotifications(
      List<AggregateTransaction> aggCompleteTxs) {
    List<AggregateTransaction> documentSigningNotifications = [];
    aggCompleteTxs.forEach((aggTx) {
      Transaction checkTx = aggTx.innerTransactions[0];
      bool isNotification = false;
      if (SimpleParsedTransaction.fromTransaction(checkTx).type ==
          TransactionType.transfer.value) {
        TransferTransaction tftx = checkTx;
        SiriusSignMessage info =
            this.parseSiriusSignMessage(tftx.message.toJson()['payload']);
        if ((info != null) &&
            (this.checkStrict(info.header.appID)) &&
            (info.header.messageType == SiriusSignMessageType.SIGN_NOTIFY))
          isNotification = true;
      }

      if (isNotification) {
        documentSigningNotifications.add(aggTx);
      }
    });
    return documentSigningNotifications;
  }

  /// Filter Document Signing Transaction from an array of transactions
  /// @param aggCompleteTxs
  @protected
  List<TransferTransaction> filterDocumentSigningTransations(
      List<Transaction> txs) {
    List<TransferTransaction> documentSigningTransactions = [];
    txs.forEach((tx) {
      if (SimpleParsedTransaction.fromTransaction(tx).type ==
          TransactionType.transfer.value) {
        TransferTransaction tftx = tx;
        SiriusSignMessage info =
            this.parseSiriusSignMessage(tftx.message.toJson()['payload']);
        if ((info != null) &&
            (this.checkStrict(info.header.appID)) &&
            ((info.header.messageType == SiriusSignMessageType.SIGN) ||
                (info.header.messageType == SiriusSignMessageType.VERIFY))) {
          documentSigningTransactions = [
            ...documentSigningTransactions,
            tx
          ]; // <TransferTransaction>
        }
      }
    });
    return documentSigningTransactions;
  }

  /// Fetch signer public key, address of cosigners and verifiers from agg noti tx
  /// @param aggNotiTx
  @protected
  Future<Map<String, List<RawAccountInfo>>>
      fetchDoucmentSignersAndVerifiersFromNotiTx(
          AggregateTransaction aggNotiTx) async {
    List<TransferTransaction> notiTxs =
        aggNotiTx.innerTransactions.cast<TransferTransaction>().toList();

    List<Address> recvAddresses = notiTxs.map((tx) => tx.recipient).toList();
    List<AccountInfo> recvInfos =
        await this.client.account.getAccountsInfo(recvAddresses);
    List<RawAccountInfo> recvs = recvInfos.map((info) {
      return RawAccountInfo(info.publicKey, info.address);
    }).toList();

    List<Address> cosignerAddresses = [];
    List<Address> verifierAddresses = [];
    notiTxs.forEach((tx) {
      SiriusSignMessage ssMsg =
          this.parseSiriusSignMessage(tx.message.toJson()['payload']);
      if (ssMsg.header.messageType == SiriusSignMessageType.SIGN_NOTIFY)
        cosignerAddresses.add(tx.recipient);
      if (ssMsg.header.messageType == SiriusSignMessageType.VERIFY_NOTIFY)
        verifierAddresses.add(tx.recipient);
    });

    List<RawAccountInfo> cosigners = recvs
        .where((recv) => cosignerAddresses
            .map((addr) => addr.address)
            .toList()
            .contains(recv.address.address))
        .toList();
    List<RawAccountInfo> verifiers = recvs
        .where((recv) => verifierAddresses
            .map((addr) => addr.address)
            .toList()
            .contains(recv.address.address))
        .toList();

    return {"cosigners": cosigners, "verifiers": verifiers};
  }

  /// Fetch info of signer and verifier from noti aggregate tx
  /// @param notiAggTx
  /// @param userAccount
  @protected
  Future<Map<String, List<SignerInfo>>> getCosignerVerifiersFromNotiAggTx(
      AggregateTransaction notiAggTx,
      [PublicAccount userAccount]) async {
    // Check cosigners
    List<SignerInfo> cosigners = [];
    List<SignerInfo> verifiers = [];
    List<RawAccountInfo> cosignerInfos = [];
    List<RawAccountInfo> verifierInfos = [];
    if (notiAggTx != null) {
      final signersInfo =
          await this.fetchDoucmentSignersAndVerifiersFromNotiTx(notiAggTx);
      cosignerInfos = signersInfo["cosigners"];
      verifierInfos = signersInfo["verifiers"];
      cosigners = cosignerInfos.map((info) {
        SignerInfo cosigner = SignerInfo(
            info.publicKey, info.address.address, false, null, null, null);
        if ((userAccount != null) &&
            (cosigner.publicKey == PUBLIC_KEY_NOT_FOUND) &&
            (cosigner.address == userAccount.address.address)) {
          cosigner.publicKey = userAccount.publicKey;
        }
        return cosigner;
      }).toList();
      verifiers = verifierInfos.map((info) {
        SignerInfo verifier = SignerInfo(
            info.publicKey, info.address.address, false, null, null, null);
        if ((userAccount != null) &&
            (verifier.publicKey == PUBLIC_KEY_NOT_FOUND) &&
            (verifier.address == userAccount.address.address)) {
          verifier.publicKey = userAccount.publicKey;
        }
        return verifier;
      }).toList();
    }

    return {"cosigners": cosigners, "verifiers": verifiers};
  }

  /// Get signed info: signer pulickey, verified publickey, owner info, signature upload tx hash
  /// from incoming transactions of a document account
  /// @param incomingTxs
  @protected
  Map<String, dynamic> getSignedSignerInfo(List<Transaction> incomingTxs,
      [PublicAccount userAccount]) {
    // Filter SIGN transactions to get signatures
    SignerInfo owner;
    SiriusSignDocumentSigningMessage ownerSigningMsg;
    TransferTransaction ownerSigningTx;
    TransferTransaction userSigningTx = null;
    List<SignerInfo> cosigners = [];
    List<SignerInfo> verifiers = [];
    incomingTxs.forEach((tx) {
      if (SimpleParsedTransaction.fromTransaction(tx).type ==
          TransactionType.transfer.value) {
        TransferTransaction transferTx = tx as TransferTransaction;
        SiriusSignMessage txMsg =
            this.parseSiriusSignMessage(transferTx.message.toJson()['payload']);
        if ((txMsg != null) &&
            (txMsg.header.messageType == SiriusSignMessageType.SIGN)) {
          SiriusSignDocumentSigningMessage signTxMsg = txMsg;
          SignerInfo signer = SignerInfo(
              transferTx.signer.publicKey,
              transferTx.signer.address.address,
              true,
              transferTx.deadline.value
                  .add(Duration(hours: DEADLINE_ADJUSTED_HOURS)),
              transferTx.transactionHash,
              signTxMsg.body["signatureUploadTxHash"]);
          if (signTxMsg.body["isOwner"]) {
            owner = signer;
            ownerSigningMsg = signTxMsg;
            ownerSigningTx = transferTx;
          } else {
            cosigners.add(signer);
          }
          if ((userAccount != null) &&
              (transferTx.signer.publicKey == userAccount.publicKey))
            userSigningTx = transferTx;
        }
        if ((txMsg != null) &&
            (txMsg.header.messageType == SiriusSignMessageType.VERIFY)) {
          SiriusSignDocumentVerifyingMessage verifyTxMsg = txMsg;
          SignerInfo verifier = SignerInfo(
              transferTx.signer.publicKey,
              transferTx.signer.address.address,
              true,
              transferTx.deadline.value
                  .add(Duration(hours: DEADLINE_ADJUSTED_HOURS)),
              transferTx.transactionHash,
              null);
          verifiers.add(verifier);
        }
      }
    });
    return {
      "owner": owner,
      "ownerSigningMsg": ownerSigningMsg,
      "ownerSigningTx": ownerSigningTx,
      "userSigningTx": userSigningTx,
      "cosigners": cosigners,
      "verifiers": verifiers
    };
  }

  /// Update signers info whow signed doc
  /// @param signers
  /// @param signeds
  @protected
  List<SignerInfo> updateSigners(
      List<SignerInfo> signers, List<SignerInfo> signeds) {
    signers.forEach((signer) {
      int idx = signeds
          .map((signed) => signed.address)
          .toList()
          .indexOf(signer.address);
      if (idx < 0) return;
      signer.isSigned = signeds[idx].isSigned;
      signer.signDate = signeds[idx].signDate;
      signer.signTxHash = signeds[idx].signTxHash;
      signer.signatureUploadTxHash = signeds[idx].signatureUploadTxHash;
    });
    return signers;
  }
}
