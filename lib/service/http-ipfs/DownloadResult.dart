part of siriussign_sdk.ipfs;

class DownloadResult {
  String transactionHash;
  int privacyType;
  String version;
  DownloadResultData data;

  DownloadResult(
    this.transactionHash,
    this.privacyType,
    this.version,
    this.data,
  );
}
