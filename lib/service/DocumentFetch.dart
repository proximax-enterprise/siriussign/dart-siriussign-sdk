part of siriussign_sdk;

class DocumentFetch extends DocumentInfoService {
  bool isFetching;

  DocumentFetch(String appID, String apiNode, int networkType,
      [int queryPageSize = 10])
      : super(apiNode, networkType, queryPageSize) {
    this.isFetching = false;
    this.setStrictFilter(appID);
  }

  /// Fetch document signing notifications
  Future<Map<String, dynamic>> documentSigningNotifications(
      PublicAccount userAccount, String lastHash) async {
    String lastId = await this.fetchTransactionId(lastHash);
    // Fetch all incomming transaction of user account
    Map<String, dynamic> aggCompleteTxsFromConfirmedTxs =
        await this.fetchAggregateCommpleteTransactions(userAccount, lastId);
    List<AggregateTransaction> aggCompleteTxs =
        aggCompleteTxsFromConfirmedTxs["txs"];
    String newLastHash = aggCompleteTxsFromConfirmedTxs["lastHash"];
    // Filter document signing notificaion transaction - Aggregate Complete Tx with NOTIFY TransferTx inner
    List<AggregateTransaction> documentSigningNotifications =
        this.filterDocumentSigningNotifications(aggCompleteTxs);
    return {"txs": documentSigningNotifications, "lastHash": newLastHash};
  }

  /// Fetch document signing transactions of a user by user's account
  /// @param userAccount
  Future<Map<String, dynamic>> documentSigningTransactionsByUserAccount(
      PublicAccount userAccount, String lastHash) async {
    String lastId = await this.fetchTransactionId(lastHash);
    List<Transaction> outgoingTxs =
        await this.fetchOutgoingTransactions(userAccount, lastId);
    List<TransferTransaction> docSigningTxs =
        this.filterDocumentSigningTransations(outgoingTxs);
    String newLastHash = outgoingTxs.length > 0
        ? SimpleParsedTransaction.fromTransaction(
                outgoingTxs[outgoingTxs.length - 1])
            .transactionHash
        : null;
    return {"txs": docSigningTxs, "lastHash": newLastHash};
  }

  /// Fetch a Sirius Sign document by document account publickey
  Future<SiriusSignDocumentInfo> byDocumentAccount(String documentPublicKey,
      [PublicAccount userAccount]) async {
    PublicAccount docPublicAcc =
        PublicAccount.fromPublicKey(documentPublicKey, this.networkType);

    // Fetch all outgoing transactions of document account and filter NOTI transaction to get signers and verifiers
    List<Transaction> docOutgoingTxs =
        await this.fetchAllOutgoingTransactions(docPublicAcc);
    List<AggregateTransaction> docAggTxs = docOutgoingTxs
        .where((outTx) =>
            SimpleParsedTransaction.fromTransaction(outTx).type ==
            TransactionType.aggregateCompleted.value)
        .toList()
        .cast<AggregateTransaction>();
    List<AggregateTransaction> notiAggTxs =
        this.filterDocumentSigningNotifications(docAggTxs);
    List<SignerInfo> cosigners = [];
    List<SignerInfo> verifiers = [];
    if (notiAggTxs.length > 0) {
      AggregateTransaction notiAggTx = notiAggTxs[0];
      Map<String, List<SignerInfo>> cosignersVerifiers =
          await this.getCosignerVerifiersFromNotiAggTx(notiAggTx, userAccount);
      cosigners = cosignersVerifiers["cosigners"];
      verifiers = cosignersVerifiers["verifiers"];
    }

    // Fetch all incomming transactions of document account and filter SIGN and VERIFY transactions to get signatures
    List<Transaction> docIncomingTxs =
        await this.fetchAllIncomingTransactions(docPublicAcc);
    Map<String, dynamic> signedInfo =
        this.getSignedSignerInfo(docIncomingTxs, userAccount);

    SignerInfo owner = signedInfo["owner"];
    TransferTransaction ownerSigningTx = signedInfo["ownerSigningTx"];
    SiriusSignDocumentSigningMessage ownerSigningMsg =
        signedInfo["ownerSigningMsg"];
    List<SignerInfo> cosigneds = signedInfo["cosigners"];
    List<SignerInfo> verifieds = signedInfo["verifiers"];
    this.updateSigners(cosigners, cosigneds);
    this.updateSigners(verifiers, verifieds);
    DateTime uploadDate = ownerSigningTx.deadline.value
        .add(Duration(hours: DEADLINE_ADJUSTED_HOURS));

    SiriusSignDocumentInfo doc = SiriusSignDocumentInfo.create(
      docPublicAcc.address.address,
      ownerSigningMsg.body["fileName"],
      ownerSigningMsg.body["fileType"],
      ownerSigningMsg.body["fileHash"],
      uploadDate,
      owner,
      cosigners,
      docPublicAcc,
      ownerSigningMsg.body["uploadTxHash"],
      ownerSigningMsg.body["encryptType"],
      verifiers,
    );
    return doc;
  }

  /// Fetch a Sirius Sign document by signing transction hash
  /// @param hash
  Future<SiriusSignDocumentInfo> bySigningTxHash(String hash) async {
    Transaction signTx = await this.fetchTransaction(hash).catchError((err) {
      return null;
    });
    if (signTx == null) return null;
    if (SimpleParsedTransaction.fromTransaction(signTx).type !=
        TransactionType.transfer.value) return null;
    TransferTransaction tx = signTx;
    SiriusSignDocumentSigningMessage info =
        this.parseSiriusSignMessage(tx.message.payload.toString());
    PublicAccount docPublicAcc = await this.fetchPublicAccount(tx.recipient);

    // Fetch all outgoing transactions of document account and filter NOTI transaction to get signers and verifiers
    List<Transaction> docOutgoingTxs =
        await this.fetchAllOutgoingTransactions(docPublicAcc);
    List<AggregateTransaction> docAggTxs = docOutgoingTxs
        .where((outTx) =>
            SimpleParsedTransaction.fromTransaction(outTx).type ==
            TransactionType.aggregateCompleted.value)
        .cast<AggregateTransaction>()
        .toList();
    List<AggregateTransaction> notiAggTxs =
        this.filterDocumentSigningNotifications(docAggTxs);
    List<SignerInfo> cosigners = [];
    List<SignerInfo> verifiers = [];
    if (notiAggTxs.length > 0) {
      AggregateTransaction notiAggTx = notiAggTxs[0];
      Map<String, List<SignerInfo>> cosignersVerifiers =
          await this.getCosignerVerifiersFromNotiAggTx(notiAggTx);
      cosigners = cosignersVerifiers["cosigners"];
      verifiers = cosignersVerifiers["verifiers"];
    }

    // Fetch all incomming transactions of document account and filter SIGN and VERIFY transactions to get signatures
    List<Transaction> docIncomingTxs =
        await this.fetchAllIncomingTransactions(docPublicAcc);
    Map<String, dynamic> signedInfo = this.getSignedSignerInfo(docIncomingTxs);

    SignerInfo owner = signedInfo["owner"];
    TransferTransaction ownerSigningTx = signedInfo["ownerSigningTx"];
    SiriusSignDocumentSigningMessage ownerSigningMsg =
        signedInfo["ownerSigningMsg"];
    List<SignerInfo> cosigneds = signedInfo["cosigners"];
    List<SignerInfo> verifieds = signedInfo["verifiers"];
    this.updateSigners(cosigners, cosigneds);
    this.updateSigners(verifiers, verifieds);
    DateTime uploadDate = ownerSigningTx.deadline.value
        .add(Duration(hours: DEADLINE_ADJUSTED_HOURS));

    SiriusSignDocumentInfo doc = SiriusSignDocumentInfo.create(
      docPublicAcc.address.address,
      ownerSigningMsg.body["fileName"],
      ownerSigningMsg.body["fileType"],
      ownerSigningMsg.body["fileHash"],
      uploadDate,
      owner,
      cosigners,
      docPublicAcc,
      ownerSigningMsg.body["uploadTxHash"],
      ownerSigningMsg.body["encryptType"],
      verifiers,
    );
    return doc;
  }

  /// Fetch need-sign and need-verify documents info from a page of transactions
  /// @param userAccount PublicAccount
  /// @param lastHash hash of the latest transaction of a page
  Future<Map<String, dynamic>> needSignVerify(
      PublicAccount userAccount, String lastHash) async {
    List<SiriusSignDocumentInfo> needSign = [];
    List<SiriusSignDocumentInfo> needVerify = [];
    Map<String, dynamic> docSigningNotiTxsFromConfirmedTxs = await this
        .documentSigningNotifications(userAccount, lastHash)
        .catchError((err) {
      print(err);
      return {"txs": [], "lastHash": null};
    });
    List<AggregateTransaction> notiAggTxs =
        docSigningNotiTxsFromConfirmedTxs["txs"];
    String newLastHash = docSigningNotiTxsFromConfirmedTxs["lastHash"];
    var fetchNeedSign = () async {
      await Util.asyncForEach(notiAggTxs, (dynamic element, int index,
          [List<dynamic> listOfElements]) async {
        AggregateTransaction notiAggTx = element as AggregateTransaction;
        //Fetch all incomming transactions of document account
        PublicAccount docPublicAcc =
            await this.fetchPublicAccount(notiAggTx.signer.address);
        List<Transaction> incomingTxs =
            await this.fetchAllIncomingTransactions(docPublicAcc);

        // Filter SIGN transactions to get signatures
        Map<String, dynamic> signedInfo =
            this.getSignedSignerInfo(incomingTxs, userAccount);

        SignerInfo owner = signedInfo["owner"];
        TransferTransaction ownerSigningTx = signedInfo["ownerSigningTx"];
        SiriusSignDocumentSigningMessage ownerSigningMsg =
            signedInfo["ownerSigningMsg"];
        List<SignerInfo> cosigneds = signedInfo["cosigners"];
        List<SignerInfo> verifieds = signedInfo["verifiers"];
        DateTime uploadDate = ownerSigningTx.deadline.value
            .add(Duration(hours: DEADLINE_ADJUSTED_HOURS));
        bool isSigned = [owner, ...cosigneds, ...verifieds]
            .map((signer) => signer.address)
            .contains(userAccount.address.address);

        if (!isSigned) {
          // Check cosigners
          Map<String, List<SignerInfo>> cosignersVerifiers = await this
              .getCosignerVerifiersFromNotiAggTx(notiAggTx, userAccount);
          List<SignerInfo> cosigners = cosignersVerifiers["cosigners"];
          List<SignerInfo> verifiers = cosignersVerifiers["verifiers"];
          this.updateSigners(cosigners, cosigneds);
          this.updateSigners(verifiers, verifieds);

          SiriusSignDocumentInfo doc = SiriusSignDocumentInfo.create(
              notiAggTx.signer.address.address,
              ownerSigningMsg.body["fileName"],
              ownerSigningMsg.body["fileType"],
              ownerSigningMsg.body["fileHash"],
              uploadDate,
              owner,
              cosigners,
              notiAggTx.signer,
              ownerSigningMsg.body["uploadTxHash"],
              ownerSigningMsg.body["encryptType"],
              verifiers);

          bool isNeedSign = cosigners
              .map((cosigner) => cosigner.address)
              .contains(userAccount.address.address);
          bool isNeedVerify = verifiers
              .map((verifier) => verifier.address)
              .contains(userAccount.address.address);
          if (isNeedSign) needSign.add(doc);
          if (isNeedVerify) needVerify.add(doc);
        }

        await Util.sleep(200);
        if (index > 0 && (index % 2) == 0) {
          await Util.sleep(200);
        }
      });
    };
    await fetchNeedSign().catchError((err) {
      print(err);
    });

    return {
      "needSign": needSign,
      "needVerify": needVerify,
      "nextHash": newLastHash
    };
  }

  /// Fetch completely signed, partially signed, verified, verifying documents info from a page of transactions
  /// @param userAccount PublicAccount
  /// @param lastHash hash of the latest transaction of a page
  Future<Map<String, dynamic>> completedAndWaiting(
      PublicAccount userAccount, String lastHash) async {
    List<SiriusSignDocumentInfo> waiting = [];
    List<SiriusSignDocumentInfo> completed = [];
    List<SiriusSignDocumentInfo> verifying = [];
    List<SiriusSignDocumentInfo> verified = [];
    Map<String, dynamic> docSignTxsFromOutgoingTxs = await this
        .documentSigningTransactionsByUserAccount(userAccount, lastHash);
    List<TransferTransaction> signTxs = docSignTxsFromOutgoingTxs["txs"];
    String newLastHash = docSignTxsFromOutgoingTxs["lastHash"];
    var fetchDocuments = () async {
      await Util.asyncForEach(signTxs, (dynamic element, int index,
          [List<dynamic> listOfElements]) async {
        TransferTransaction tx = element as TransferTransaction;
        SiriusSignDocumentSigningMessage info = this.parseSiriusSignMessage(
            SimpleParsedTransaction.fromTransaction(tx)
                .message
                .toJson()['payload']);
        PublicAccount docPublicAcc =
            await this.fetchPublicAccount(tx.recipient);

        // Fetch noti agg transaction
        List<Transaction> docOutgoingTxs =
            await this.fetchAllOutgoingTransactions(docPublicAcc);
        List<AggregateTransaction> docAggTxs = docOutgoingTxs
            .where((outTx) =>
                outTx.toJson()['abstractTransaction']['type'] ==
                TransactionType.aggregateCompleted.value)
            .cast<AggregateTransaction>()
            .toList();
        List<AggregateTransaction> notiAggTxs =
            this.filterDocumentSigningNotifications(docAggTxs);

        // Fetch cosigners and verifiers
        List<SignerInfo> cosigners = [];
        List<SignerInfo> verifiers = [];
        if (notiAggTxs.length > 0) {
          AggregateTransaction notiAggTx = notiAggTxs[0];
          Map<String, List<SignerInfo>> cosignersVerifiers = await this
              .getCosignerVerifiersFromNotiAggTx(notiAggTx, userAccount);
          cosigners = cosignersVerifiers["cosigners"];
          verifiers = cosignersVerifiers["verifiers"];
        }

        //Fetch all incomming transactions of document account
        List<Transaction> incomingTxs =
            await this.fetchAllIncomingTransactions(docPublicAcc);

        // Filter SIGN and VERIFY transactions to get signatures
        Map<String, dynamic> signedInfo =
            this.getSignedSignerInfo(incomingTxs, userAccount);

        SignerInfo owner = signedInfo["owner"];
        TransferTransaction ownerSigningTx = signedInfo["ownerSigningTx"];
        SiriusSignDocumentSigningMessage ownerSigningMsg =
            signedInfo["ownerSigningMsg"];
        List<SignerInfo> cosigneds = signedInfo["cosigners"];
        List<SignerInfo> verifieds = signedInfo["verifiers"];
        this.updateSigners(cosigners, cosigneds);
        this.updateSigners(verifiers, verifieds);

        DateTime uploadDate = ownerSigningTx.deadline.value
            .add(Duration(hours: DEADLINE_ADJUSTED_HOURS));

        SiriusSignDocumentInfo doc = SiriusSignDocumentInfo.create(
          docPublicAcc.address.address,
          ownerSigningMsg.body["fileName"],
          ownerSigningMsg.body["fileType"],
          ownerSigningMsg.body["fileHash"],
          uploadDate,
          owner,
          cosigners,
          docPublicAcc,
          ownerSigningMsg.body["uploadTxHash"],
          ownerSigningMsg.body["encryptType"],
          verifiers,
        );
        bool isSigner = doc.checkIsSigner(userAccount.address.address);
        bool isVerifier = doc.checkIsVerifier(userAccount.address.address);
        if (isSigner) {
          if (doc.checkIsCompleted())
            completed.add(doc);
          else
            waiting.add(doc);
        }
        if (isVerifier) {
          if (doc.checkIsVerified())
            verified.add(doc);
          else
            verifying.add(doc);
        }

        await Util.sleep(200);
        if (index > 0 && (index % 2) == 0) {
          await Util.sleep(200);
        }
      });
    };
    await fetchDocuments().catchError((err) {
      print(err);
    });

    return {
      "completed": completed,
      "waiting": waiting,
      "verified": verified,
      "verifying": verifying,
      "nextHash": newLastHash
    };
  }

  /// Fetch all document signing notifications
  Future<List<AggregateTransaction>> allDocumentSigningNotifications(
      PublicAccount userAccount) async {
    // Fetch all incomming transaction of user account
    List<AggregateTransaction> allAggCompleteTxs =
        await this.fetchAllAggregateCommpleteTransactions(userAccount);
    // Filter document signing notificaion transaction - Aggregate Complete Tx with NOTIFY TransferTx inner
    List<AggregateTransaction> documentSigningNotifications =
        this.filterDocumentSigningNotifications(allAggCompleteTxs);
    return documentSigningNotifications;
  }

  /// Fetch all document signing transactions of a document by its account
  /// @param documentPublicKey
  Future<List<TransferTransaction>>
      allDocumentSigningTransactionsByDocumentAccount(
          String documentPublicKey) async {
    PublicAccount documentPublicAccount =
        PublicAccount.fromPublicKey(documentPublicKey, this.networkType);
    List<TransferTransaction> allDocSigningTxs = [];
    await this.fetchAll(
        documentPublicAccount,
        false,
        (PublicAccount publicAccount, String lastId) =>
            this.fetchIncomingTransactions(publicAccount, lastId),
        (List<Transaction> txs) {
      txs.forEach((tx) {
        if (SimpleParsedTransaction.fromTransaction(tx).type ==
            TransactionType.transfer.value) {
          SiriusSignMessage info = this.parseSiriusSignMessage(
              (tx as TransferTransaction).message.toJson()['payload']);
          if ((info != null) &&
              (this.checkStrict(info.header.appID)) &&
              ((info.header.messageType == SiriusSignMessageType.SIGN))) {
            allDocSigningTxs = [...allDocSigningTxs, tx];
          }
        }
      });
    });
    return allDocSigningTxs;
  }

  /// Fetch all document signing transactions of a user by user's account
  /// @param userAccount
  Future<List<TransferTransaction>> allDocumentSigningTransactionsByUserAccount(
      PublicAccount userAccount) async {
    List<TransferTransaction> allDocSigningTxs = [];
    await this.fetchAll(
        userAccount,
        false,
        (PublicAccount publicAccount, String lastId) =>
            this.fetchOutgoingTransactions(publicAccount, lastId),
        (List<Transaction> txs) {
      txs.forEach((tx) {
        if (SimpleParsedTransaction.fromTransaction(tx).type ==
            TransactionType.transfer.value) {
          SiriusSignMessage info = this.parseSiriusSignMessage(
              SimpleParsedTransaction.fromTransaction(tx)
                  .message
                  .toJson()['payload']);
          if ((info != null) &&
              (info.header.appID == this.appID) &&
              ((info.header.messageType == SiriusSignMessageType.SIGN) ||
                  (info.header.messageType == SiriusSignMessageType.VERIFY))) {
            allDocSigningTxs = [...allDocSigningTxs, tx];
          }
        }
      });
    });
    return allDocSigningTxs;
  }

  /// Fetch all document that need user to sign
  Future<Map<String, List<SiriusSignDocumentInfo>>> allNeedSignVerify(
      PublicAccount userAccount,
      [dynamic fnStopFetching]) async {
    var fnStopFetchingCallback;
    if (fnStopFetching != null)
      fnStopFetchingCallback = fnStopFetching;
    else
      fnStopFetchingCallback = () => false;

    if (this.isFetching) return null;
    this.isFetching = true;
    List<SiriusSignDocumentInfo> needSign = [];
    List<SiriusSignDocumentInfo> needVerify = [];
    List<AggregateTransaction> notiAggTxs = await this
        .allDocumentSigningNotifications(userAccount)
        .catchError((err) {
      print(err);
      this.isFetching = false;
      return [];
    });
    var fetchNeedSign = () async {
      await Util.asyncForEach(notiAggTxs, (dynamic element, int index,
          [List<dynamic> listOfElements]) async {
        AggregateTransaction notiAggTx = element as AggregateTransaction;
        //Fetch all incomming transactions of document account
        PublicAccount docPublicAcc =
            await this.fetchPublicAccount(notiAggTx.signer.address);
        List<Transaction> incomingTxs =
            await this.fetchAllIncomingTransactions(docPublicAcc);

        // Filter SIGN transactions to get signatures
        final signedInfo = this.getSignedSignerInfo(incomingTxs, userAccount);

        SignerInfo owner = signedInfo["owner"];
        TransferTransaction ownerSigningTx = signedInfo["ownerSigningTx"];
        SiriusSignDocumentSigningMessage ownerSigningMsg =
            signedInfo["ownerSigningMsg"];
        List<SignerInfo> cosigneds = signedInfo["cosigners"];
        List<SignerInfo> verifieds = signedInfo["verifiers"];
        DateTime uploadDate = ownerSigningTx.deadline.value
            .add(Duration(hours: DEADLINE_ADJUSTED_HOURS));
        bool isSigned = [owner, ...cosigneds, ...verifieds]
            .map((signer) => signer.address)
            .contains(userAccount.address.address);

        if (!isSigned) {
          // Check cosigners
          final cosignersVerifiers = await this
              .getCosignerVerifiersFromNotiAggTx(notiAggTx, userAccount);
          List<SignerInfo> cosigners = cosignersVerifiers["cosigners"];
          List<SignerInfo> verifiers = cosignersVerifiers["verifiers"];
          this.updateSigners(cosigners, cosigneds);
          this.updateSigners(verifiers, verifieds);

          SiriusSignDocumentInfo doc = SiriusSignDocumentInfo.create(
              notiAggTx.signer.address.address,
              ownerSigningMsg.body["fileName"],
              ownerSigningMsg.body["fileType"],
              ownerSigningMsg.body["fileHash"],
              uploadDate,
              owner,
              cosigners,
              notiAggTx.signer,
              ownerSigningMsg.body["uploadTxHash"],
              ownerSigningMsg.body["encryptType"],
              verifiers);

          bool isNeedSign = cosigners
              .map((cosigner) => cosigner.address)
              .contains(userAccount.address.address);
          bool isNeedVerify = verifiers
              .map((verifier) => verifier.address)
              .contains(userAccount.address.address);
          if (isNeedSign) needSign.add(doc);
          if (isNeedVerify) needVerify.add(doc);
        }

        await Util.sleep(200);
        if (index > 0 && (index % 2) == 0) {
          await Util.sleep(200);
        }
      }, () => fnStopFetchingCallback());
    };
    await fetchNeedSign().catchError((err) {
      print(err);
      this.isFetching = false;
    });

    if (fnStopFetchingCallback()) {
      this.isFetching = false;
      return null;
    }

    this.isFetching = false;
    return {"needSign": needSign, "needVerify": needVerify};
  }

  /// Fetch all document info that user account is a co-signer but has not signed yet
  Future<List<SiriusSignDocumentInfo>> allNeedSign(PublicAccount userAccount,
      [dynamic fnStopFetching]) async {
    var fnStopFetchingCallback;
    if (fnStopFetching != null)
      fnStopFetchingCallback = fnStopFetching;
    else
      fnStopFetchingCallback = () => false;
    final needs =
        await this.allNeedSignVerify(userAccount, fnStopFetchingCallback);
    return needs["needSign"];
  }

  /// Fetch all document info that user account is a verifier but has not verified yet
  Future<List<SiriusSignDocumentInfo>> allNeedVerify(PublicAccount userAccount,
      [dynamic fnStopFetching]) async {
    var fnStopFetchingCallback;
    if (fnStopFetching != null)
      fnStopFetchingCallback = fnStopFetching;
    else
      fnStopFetchingCallback = () => false;
    final needs =
        await this.allNeedSignVerify(userAccount, fnStopFetchingCallback);
    return needs["needVerify"];
  }

  /// Fetch all compeleted and waiting documents
  Future<Map<String, List<SiriusSignDocumentInfo>>> allCompletedAndWaiting(
      PublicAccount userAccount,
      [dynamic fnStopFetching]) async {
    var fnStopFetchingCallback;
    if (fnStopFetching != null)
      fnStopFetchingCallback = fnStopFetching;
    else
      fnStopFetchingCallback = () => false;
    if (this.isFetching) return null;
    this.isFetching = true;
    List<SiriusSignDocumentInfo> waiting = [];
    List<SiriusSignDocumentInfo> completed = [];
    List<SiriusSignDocumentInfo> verifying = [];
    List<SiriusSignDocumentInfo> verified = [];
    List<TransferTransaction> signTxs =
        await this.allDocumentSigningTransactionsByUserAccount(userAccount);
    var fetchDocuments = () async {
      await Util.asyncForEach(signTxs, (dynamic element, int index,
          [List<dynamic> listOfElements]) async {
        TransferTransaction tx = element as TransferTransaction;
        // SiriusSignDocumentSigningMessage info = this.parseSiriusSignMessage((SimpleParsedTransaction.fromTransaction(tx).message.payload.toString());
        PublicAccount docPublicAcc =
            await this.fetchPublicAccount(tx.recipient);

        // Fetch noti agg transaction
        List<Transaction> docOutgoingTxs =
            await this.fetchAllOutgoingTransactions(docPublicAcc);
        List<AggregateTransaction> docAggTxs = docOutgoingTxs
            .where((outTx) =>
                SimpleParsedTransaction.fromTransaction(outTx).type ==
                TransactionType.aggregateCompleted.value)
            .cast<AggregateTransaction>()
            .toList();
        List<AggregateTransaction> notiAggTxs =
            this.filterDocumentSigningNotifications(docAggTxs);

        // Fetch cosigners and verifiers
        List<SignerInfo> cosigners = [];
        List<SignerInfo> verifiers = [];
        if (notiAggTxs.length > 0) {
          AggregateTransaction notiAggTx = notiAggTxs[0];
          final cosignersVerifiers = await this
              .getCosignerVerifiersFromNotiAggTx(notiAggTx, userAccount);
          cosigners = cosignersVerifiers["cosigners"];
          verifiers = cosignersVerifiers["verifiers"];
        }

        //Fetch all incomming transactions of document account
        List<Transaction> incomingTxs =
            await this.fetchAllIncomingTransactions(docPublicAcc);

        // Filter SIGN and VERIFY transactions to get signatures
        final signedInfo = this.getSignedSignerInfo(incomingTxs, userAccount);

        SignerInfo owner = signedInfo["owner"];
        TransferTransaction ownerSigningTx = signedInfo["ownerSigningTx"];
        SiriusSignDocumentSigningMessage ownerSigningMsg =
            signedInfo["ownerSigningMsg"];
        List<SignerInfo> cosigneds = signedInfo["cosigners"];
        List<SignerInfo> verifieds = signedInfo["verifiers"];
        this.updateSigners(cosigners, cosigneds);
        this.updateSigners(verifiers, verifieds);

        DateTime uploadDate = ownerSigningTx.deadline.value
            .add(Duration(hours: DEADLINE_ADJUSTED_HOURS));

        SiriusSignDocumentInfo doc = SiriusSignDocumentInfo.create(
          docPublicAcc.address.address,
          ownerSigningMsg.body["fileName"],
          ownerSigningMsg.body["fileType"],
          ownerSigningMsg.body["fileHash"],
          uploadDate,
          owner,
          cosigners,
          docPublicAcc,
          ownerSigningMsg.body["uploadTxHash"],
          ownerSigningMsg.body["encryptType"],
          verifiers,
        );
        bool isSigner = doc.checkIsSigner(userAccount.address.address);
        bool isVerifier = doc.checkIsVerifier(userAccount.address.address);
        if (isSigner) {
          if (doc.checkIsCompleted())
            completed.add(doc);
          else
            waiting.add(doc);
        }
        if (isVerifier) {
          if (doc.checkIsVerified())
            verified.add(doc);
          else
            verifying.add(doc);
        }

        await Util.sleep(200);
        if (index > 0 && (index % 2) == 0) {
          await Util.sleep(200);
        }
      }, () => fnStopFetchingCallback());
    };
    await fetchDocuments().catchError((err) {
      print(err);
      this.isFetching = false;
    });
    if (fnStopFetchingCallback()) return null;
    this.isFetching = false;

    return {
      "completed": completed,
      "waiting": waiting,
      "verified": verified,
      "verifying": verifying
    };
  }

  /// Fetch all completely signed documents info
  /// @param fnStopFetching callback return true to stop fetching
  Future<List<SiriusSignDocumentInfo>> allCompleted(PublicAccount userAccount,
      [dynamic fnStopFetching]) async {
    var fnStopFetchingCallback;
    if (fnStopFetching != null)
      fnStopFetchingCallback = fnStopFetching;
    else
      fnStopFetchingCallback = () => false;
    final classifiedDocs =
        await this.allCompletedAndWaiting(userAccount, fnStopFetchingCallback);
    return classifiedDocs["completed"];
  }

  /// Fetch all documents info that are waiting for other account to sign
  /// @param fnStopFetching callback return true to stop fetching
  Future<List<SiriusSignDocumentInfo>> allWaiting(PublicAccount userAccount,
      [dynamic fnStopFetching]) async {
    var fnStopFetchingCallback;
    if (fnStopFetching != null)
      fnStopFetchingCallback = fnStopFetching;
    else
      fnStopFetchingCallback = () => false;
    final classifiedDocs =
        await this.allCompletedAndWaiting(userAccount, fnStopFetchingCallback);
    return classifiedDocs["waiting"];
  }

  /// Fetch all completely verified documents info
  /// @param fnStopFetching callback return true to stop fetching
  Future<List<SiriusSignDocumentInfo>> allVerified(PublicAccount userAccount,
      [dynamic fnStopFetching]) async {
    var fnStopFetchingCallback;
    if (fnStopFetching != null)
      fnStopFetchingCallback = fnStopFetching;
    else
      fnStopFetchingCallback = () => false;
    final classifiedDocs =
        await this.allCompletedAndWaiting(userAccount, fnStopFetchingCallback);
    return classifiedDocs["verified"];
  }

  /// Fetch all documents info that are waiting for other account to verify
  /// @param fnStopFetching callback return true to stop fetching
  Future<List<SiriusSignDocumentInfo>> allVerifying(PublicAccount userAccount,
      [dynamic fnStopFetching]) async {
    var fnStopFetchingCallback;
    if (fnStopFetching != null)
      fnStopFetchingCallback = fnStopFetching;
    else
      fnStopFetchingCallback = () => false;
    final classifiedDocs =
        await this.allCompletedAndWaiting(userAccount, fnStopFetchingCallback);
    return classifiedDocs["verifying"];
  }

  /// Fetch the final document upload transaction
  fetchFinalDocumentUploadTx(PublicAccount documentAccount) async {
    final allIncomingTxs =
        await this.fetchAllIncomingTransactions(documentAccount);
    for (var i = 0; i < allIncomingTxs.length; i++) {
      final tx = allIncomingTxs[i];
      if (tx.toJson()["abstractTransaction"]["type"] !=
          TransactionType.transfer) continue;
      final message = (tx as TransferTransaction).message.toJson()["payload"];
      final ssMsg = jsonDecode(message);
      if (ssMsg.privacyType && ssMsg.data && ssMsg.version) {
        if (ssMsg.data.description == 'SiriusSign Signed Document') return tx;
      }
    }
    return null;
  }
}
