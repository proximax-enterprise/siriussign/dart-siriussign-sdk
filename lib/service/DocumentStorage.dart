part of siriussign_sdk;

class DocumentStorage {
  // Dowloader, Uploader
  Uploader _uploader;
  Downloader _downloader;
  SiriusClient _siriusClient;

  DocumentStorage(String apiNode, String ipfsNode, int networkType) {
    this._uploader = new Uploader(apiNode, ipfsNode);
    this._downloader = new Downloader(apiNode, ipfsNode);
    this._siriusClient = SiriusClient.fromUrl(apiNode, null);
  }

  /// Create upload params builder
  /// @param document
  UploadParameterData _createParamsBuilder(
      String fileName,
      String fileType,
      Uint8List fileData,
      String fileHash,
      Account documentAccount,
      Account owner) {
    final encrypter = Encrypt.Encrypter(Encrypt.AES(Encrypt.Key.fromUtf8(
        owner.account.privateKey.toString().substring(0, 32))));
    final Encrypt.IV iv = Encrypt.IV.fromLength(16);
    Encrypt.Encrypted documentAccountPriv = encrypter
        .encrypt(documentAccount.account.privateKey.toString(), iv: iv);
    String documentAccountEncPriv =
        ConverterUtil.uint8ListToHex(documentAccountPriv.bytes);
    Map<String, String> metaData = {
      'SFH': fileHash,
      'SDA': documentAccountEncPriv
    };
    final metaParams =
        UploadParameterData(fileData, fileName, '', fileType, metaData);
    return metaParams;
    // final uploadParameterData =
    //     UploadParameter(metaParams, owner.account.privateKey.toString());
    // return uploadParameterData;
  }

  /// Upload file from Sirius Sign Document
  Future<SignedTransaction> uploadDocument(
      SiriusSignDocument document, Account owner,
      [String password]) async {
    final privateKey = owner.account.privateKey.toString();
    final publicKey = owner.publicKey;
    final fileName = document.fileName;
    final fileType = document.fileType;
    final fileHash = document.fileHash;
    final fileData = document.fileData;

    final uploadParameterData = this._createParamsBuilder(fileName, fileType,
        fileData, fileHash, document.documentAccount, owner);

    String generationHash = await this._siriusClient.generationHash;

    UploadParameter uploadParams;
    int encryptType = document.info.encryptType;
    switch (encryptType) {
      case EncryptType.KEYPAIR:
        {
          uploadParams = UploadParameter(
              uploadParameterData,
              privateKey,
              NemPrivacyStrategy(privateKey, publicKey),
              2,
              false,
              generationHash,
              false,
              false,
              [],
              document.documentAccount.publicKey,
              document.documentAccount.address.address);
          break;
        }
      case EncryptType.PASSWORD:
        {
          // print('EncryptType.PASSWORD is not supported yet');
          if (password == null) throw ('I02: password is required');
          uploadParams = UploadParameter(
              uploadParameterData,
              privateKey,
              PasswordPrivacyStrategy(password),
              2,
              false,
              generationHash,
              false,
              false,
              [],
              document.documentAccount.publicKey,
              document.documentAccount.address.address);
          // uploadParams = uploadParameterData
          //     .withRecipientPublicKey(document.documentAccount.publicKey)
          //     .withTransactionMosaics([])
          //     .withPasswordPrivacy(password)
          //     .build();
          break;
        }
      case EncryptType.PLAIN:
        {
          uploadParams = UploadParameter(
              uploadParameterData,
              privateKey,
              PlainPrivacyStrategy(),
              2,
              false,
              generationHash,
              false,
              false,
              [],
              document.documentAccount.publicKey,
              document.documentAccount.address.address);
          // uploadParams = uploadParameterData
          //     .withRecipientPublicKey(document.documentAccount.publicKey)
          //     .withTransactionMosaics([]).build();
          break;
        }
      default:
        {
          uploadParams = UploadParameter(
              uploadParameterData,
              privateKey,
              PlainPrivacyStrategy(),
              2,
              false,
              generationHash,
              false,
              false,
              [],
              document.documentAccount.publicKey,
              document.documentAccount.address.address);
          // uploadParams = uploadParameterData
          //     .withRecipientPublicKey(document.documentAccount.publicKey)
          //     .withTransactionMosaics([]).build();
          break;
        }
    }

    return this._uploader.upload(uploadParams);
  }

  /// Download file from file hash
  Future<DownloadResult> downloadDocument(String hash, int encryptType,
      {Account uploader, String password}) {
    // final downloadParamsBuilder = DownloadParameter.create(hash);
    DownloadParameter downloadParams;
    switch (encryptType) {
      case EncryptType.KEYPAIR:
        {
          if (uploader == null)
            throw ('I03: Owner Account as decryptParam is required');
          final owner = uploader;
          final privateKey = owner.account.privateKey.toString();
          final publicKey = owner.publicKey;

          downloadParams = DownloadParameter(
              hash, NemPrivacyStrategy(privateKey, publicKey), false, null);
          // downloadParams = downloadParamsBuilder
          //     .withNemKeysPrivacy(privateKey, publicKey)
          //     .build();
          break;
        }
      case EncryptType.PASSWORD:
        {
          if (password == null)
            throw ('I04: Password as decryptParam is required');
          // print('EncryptType.PASSWORD is not supported yet');
          downloadParams = DownloadParameter(
              hash, PasswordPrivacyStrategy(password), false, null);
          // downloadParams =
          //     downloadParamsBuilder.withPasswordPrivacy(password).build();
          break;
        }
      case EncryptType.PLAIN:
        {
          downloadParams =
              DownloadParameter(hash, PlainPrivacyStrategy(), false, null);
          // downloadParams = downloadParamsBuilder.build();
          break;
        }
      default:
        {
          downloadParams =
              DownloadParameter(hash, PlainPrivacyStrategy(), false, null);
          // downloadParams = downloadParamsBuilder.build();
          break;
        }
    }
    return this._downloader.download(downloadParams);
  }

  /// Upload signature image
  /// @param signatureImg
  uploadCompletedDoc(
      String originFilename,
      String fileType,
      String originFileHash,
      Uint8List completedFileData,
      Account uploaderAccount,
      PublicAccount documentPublicAccount,
      int encryptType,
      [String password]) async {
    final fileContent = completedFileData;
    final fileName =
        originFilename.substring(0, originFilename.lastIndexOf('.')) +
            '-' +
            'signed' +
            originFilename.substring(originFilename.lastIndexOf('.'));
    final fileDescription = 'SiriusSign Signed Document';
    final completedDocDataUri =
        'data:' + fileType + ';base64,' + base64.encode(completedFileData);
    // final cFileHash = ConverterUtil.base64ToHex(sha256.convert(utf8.encode(completedDocDataUri)).toString()).toUpperCase();
    final cFileHash = sha256
        .convert(utf8.encode(completedDocDataUri))
        .toString()
        .toUpperCase();
    final oFileHash = originFileHash;
    final Map<String, String> metaData = {'SCH': cFileHash, 'SFH': oFileHash};

    final privateKey = uploaderAccount.account.privateKey.toString();
    final publicKey = uploaderAccount.publicKey;

    final metaParams = UploadParameterData(
        fileContent, fileName, fileDescription, fileType, metaData);

    String generationHash = await this._siriusClient.generationHash;
    // final uploadParameterData =
    //     UploadParameter.createForUint8ArrayUpload(metaParams, privateKey);
    UploadParameter uploadParams;
    switch (encryptType) {
      case EncryptType.KEYPAIR:
        {
          uploadParams = UploadParameter(
              metaParams,
              privateKey,
              NemPrivacyStrategy(privateKey, publicKey),
              2,
              false,
              generationHash,
              false,
              false,
              [],
              documentPublicAccount.publicKey,
              documentPublicAccount.address.address);
          break;
        }
      case EncryptType.PASSWORD:
        {
          // print('EncryptType.PASSWORD is not supported yet');

          if (password == null) throw ('I02: password is required');
          uploadParams = UploadParameter(
              metaParams,
              privateKey,
              PasswordPrivacyStrategy(password),
              2,
              false,
              generationHash,
              false,
              false,
              [],
              documentPublicAccount.publicKey,
              documentPublicAccount.address.address);
          // uploadParams = uploadParameterData
          //     .withRecipientPublicKey(documentPublicAccount.publicKey)
          //     .withTransactionMosaics([])
          //     .withPasswordPrivacy(password)
          //     .build();
          break;
        }
      case EncryptType.PLAIN:
        {
          uploadParams = UploadParameter(
              metaParams,
              privateKey,
              PlainPrivacyStrategy(),
              2,
              false,
              generationHash,
              false,
              false,
              [],
              documentPublicAccount.publicKey,
              documentPublicAccount.address.address);
          // uploadParams = uploadParameterData
          //     .withRecipientPublicKey(documentPublicAccount.publicKey)
          //     .withTransactionMosaics([]).build();
          break;
        }
      default:
        {
          uploadParams = UploadParameter(
              metaParams,
              privateKey,
              PlainPrivacyStrategy(),
              2,
              false,
              generationHash,
              false,
              false,
              [],
              documentPublicAccount.publicKey,
              documentPublicAccount.address.address);
          // uploadParams = uploadParameterData
          //     .withRecipientPublicKey(documentPublicAccount.publicKey)
          //     .withTransactionMosaics([]).build();
          break;
        }
    }

    return this._uploader.upload(uploadParams);
  }

  /// Upload signature image
  Future<SignedTransaction> uploadSignatureForDocSigning(
      String fileName,
      String fileType,
      Uint8List fileData,
      Account userAccount,
      PublicAccount documentPublicAccount) async {
    final fileDescription = 'SiriusSign Signature Image';
    final signatureImg =
        ConverterUtil.base64ToDataURI(base64.encode(fileData), fileType);
    final fileHash =
        sha256.convert(utf8.encode(signatureImg)).toString().toUpperCase();
    final Map<String, String> metaData = {'SSH': fileHash};

    final privateKey = userAccount.account.privateKey.toString();

    final metaParams = UploadParameterData(
        fileData, fileName, fileDescription, fileType, metaData);
    // final uploadParameterData =
    //     UploadParameter.createForUint8ArrayUpload(metaParams, privateKey);
    // final uploadParams = uploadParameterData
    //     .withRecipientPublicKey(documentPublicAccount.publicKey)
    //     .withTransactionMosaics([]).build();
    String generationHash = await this._siriusClient.generationHash;

    final uploadParams = UploadParameter(
        metaParams,
        privateKey,
        PlainPrivacyStrategy(),
        2,
        false,
        generationHash,
        false,
        false,
        [],
        documentPublicAccount.publicKey,
        documentPublicAccount.address.address);

    return this._uploader.upload(uploadParams);
  }

  Future<SignedTransaction> upload(
    Account sender,
    PublicAccount receiver,
    FileInfo fileInfo,
    String decription,
    Map<String, String> metadata,
    List<Mosaic> mosaics,
  ) async {
    final fileData = fileInfo.fileData;
    final fileName = fileInfo.fileName;
    final fileType = fileInfo.fileType;
    final metaParams =
        UploadParameterData(fileData, fileName, decription, fileType, metadata);
    // final uploadParameterData = UploadParameter.createForUint8ArrayUpload(
    //     metaParams, sender.account.privateKey.toString());
    // final uploadParams = uploadParameterData
    //     .withRecipientPublicKey(receiver.publicKey)
    //     .withTransactionMosaics(mosaics)
    //     .withUseBlockchainSecureMessage(isSecureMessage)
    //     .build();
    String generationHash = await this._siriusClient.generationHash;

    final uploadParams = UploadParameter(
        metaParams,
        sender.account.privateKey.toString(),
        PlainPrivacyStrategy(),
        2,
        false,
        generationHash,
        false,
        false,
        mosaics,
        receiver.publicKey,
        receiver.address.address);

    return this._uploader.upload(uploadParams);
  }
}
