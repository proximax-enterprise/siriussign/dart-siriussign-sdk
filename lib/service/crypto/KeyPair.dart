part of siriussign_sdk.ipfs;

class KeyPair {
  /// Creates a key pair from a private key string.
  /// @param {string} privateKeyString A hex encoded private key string.
  /// @param {SignSchema} signSchema The Sign Schema. (KECCAK_REVERSED_KEY / SHA3)
  /// @returns {module:crypto/keyPair~KeyPair} The key pair.
  static Map<String, Uint8List> createKeyPairFromPrivateKeyString(
      privateKeyString,
      [signSchema = SignSchema.SHA3]) {
    final privateKey = ConverterUtil.hexToUint8Array(privateKeyString);

    // KECCAK_REVERSED_KEY uses reversed private key.
    if (signSchema == SignSchema.KECCAK_REVERSED_KEY)
      throw ('SignSchema.KECCAK_REVERSED_KEY is not supported');
    final secretKey = signSchema == SignSchema.SHA3
        ? privateKey
        : null; // convert.hexToUint8Reverse(privateKeyString);
    if (Key_Size != privateKey.length) {
      throw ('private key has unexpected size: ${privateKey.length}');
    }
    final publicKey = CatapultCrypto.extractPublicKey(
        secretKey, CatapultHash.func, signSchema);
    return {
      'privateKey': privateKey,
      'publicKey': publicKey,
    };
  }

  /// Signs a data buffer with a key pair.
  /// @param {module:crypto/keyPair~KeyPair} keyPair The key pair to use for signing.
  /// @param {Uint8Array} data The data to sign.
  /// @param {SignSchema} signSchema The Sign Schema. (KECCAK_REVERSED_KEY / SHA3)
  /// @returns {Uint8Array} The signature.
  // static sign(keyPair, data, [signSchema = SignSchema.SHA3]) {
  //     var secretKey = keyPair.privateKey;
  //     // KECCAK_REVERSED_KEY uses reversed private key.
  //     if (signSchema == SignSchema.KECCAK_REVERSED_KEY) {
  //         secretKey = convert.hexToUint8Reverse(convert.uint8ToHex(secretKey));
  //     }
  //     return Utility.catapult_crypto.sign(data, keyPair.publicKey, secretKey,
  //             Utility.catapult_hash.createHasher(64, signSchema));
  // }

  /// Verifies a signature.
  /// @param {module:crypto/keyPair~PublicKey} publicKey The public key to use for verification.
  /// @param {Uint8Array} data The data to verify.
  /// @param {Uint8Array} signature The signature to verify.
  /// @param {SignSchema} signSchema The Sign Schema. (KECCAK_REVERSED_KEY / SHA3)
  /// @returns {boolean} true if the signature is verifiable, false otherwise.
  // static verify(publicKey, data, signature, [signSchema = SignSchema.SHA3]) {
  //     return Utility.catapult_crypto.verify(publicKey, data, signature, Utility.catapult_hash.createHasher(64, signSchema));
  // }

  /// Creates a shared key given a key pair and an arbitrary public key.
  /// The shared key can be used for encrypted message passing between the two.
  /// @param {module:crypto/keyPair~KeyPair} keyPair The key pair for which to create the shared key.
  /// @param {Uint8Array} publicKey The public key for which to create the shared key.
  /// @param {Uint8Array} salt A salt that should be applied to the shared key.
  /// @param {SignSchema} signSchema The Sign Schema. (KECCAK_REVERSED_KEY / SHA3)
  /// @returns {Uint8Array} The shared key.
  static Uint8List deriveSharedKey(keyPair, publicKey, salt,
      [signSchema = SignSchema.SHA3]) {
    if (Key_Size != salt.length) {
      throw ('salt has unexpected size: ${salt.length}');
    }
    if (Key_Size != publicKey.length) {
      throw ('public key has unexpected size: ${salt.length}');
    }
    var secretKey = keyPair['privateKey'];
    // KECCAK_REVERSED_KEY uses reversed private key.
    if (signSchema == SignSchema.KECCAK_REVERSED_KEY) {
      throw ('SignSchema.KECCAK_REVERSED_KEY is not supported yet.');
      // secretKey = convert.hexToUint8Reverse(convert.uint8ToHex(secretKey));
    }
    return CatapultCrypto.deriveSharedKey(
        salt, secretKey, publicKey, CatapultHash.func, signSchema);
  }
}
