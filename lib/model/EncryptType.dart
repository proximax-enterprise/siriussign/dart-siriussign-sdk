part of siriussign_sdk;

class EncryptType {
  static const int PLAIN = 0;
  static const int KEYPAIR = 1;
  static const int PASSWORD = 2;
}
