part of siriussign_sdk.ipfs;

class UploadParameterData {
  static const int TYPE_BYTE_STREAM = 1;
  static const int TYPE_PATH = 2;
  static const int _MAX_DESCRIPTION_LENGTH = 100;
  static const int _MAX_NAME_LENGTH = 100;
  static const int _MAX_CONTENT_TYPE_LENGTH = 80;
  static const int _MAX_METADATA_JSON_LENGTH = 400;

  // int type;
  Uint8List data;
  String name;
  String description;
  String contentType;
  Map<String, String> metadata;

  UploadParameterData(
    // this.type,
    this.data, [
    this.name,
    this.description,
    this.contentType,
    this.metadata,
  ]) {
    if ((this.name != null) &&
        this.name.length > UploadParameterData._MAX_NAME_LENGTH) {
      throw ('name cannot be more than ${UploadParameterData._MAX_NAME_LENGTH} characters');
    }

    if ((this.description != null) &&
        this.description.length > UploadParameterData._MAX_DESCRIPTION_LENGTH) {
      throw ('description cannot be more than ${UploadParameterData._MAX_DESCRIPTION_LENGTH} characters');
    }

    if ((this.contentType != null) &&
        this.contentType.length >
            UploadParameterData._MAX_CONTENT_TYPE_LENGTH) {
      throw ('contentType cannot be more than ${UploadParameterData._MAX_CONTENT_TYPE_LENGTH} characters');
    }

    if ((this.metadata != null) &&
        jsonEncode(this.metadata).length >
            UploadParameterData._MAX_METADATA_JSON_LENGTH) {
      throw ('metadata cannot be more than ${UploadParameterData._MAX_METADATA_JSON_LENGTH} characters');
    }
  }

  Stream<int> getByteStream() {
    return Stream.fromIterable(this.data);
  }
}
