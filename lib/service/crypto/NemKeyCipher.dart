part of siriussign_sdk.ipfs;

class NemKeysCipher {
  String privateKey;
  String publicKey;
  Encrypt.Encrypter cipher;
  Encrypt.IV iv;
  List<int> salt = List<int>.filled(32, 0);

  NemKeysCipher(this.privateKey, this.publicKey) {
    // final salt = crypto.randomBytes(32);
    this.iv = Encrypt.IV.fromSecureRandom(16);
    this.cipher = this.getCipher();
    // this.push(salt);
    // this.push(iv);
  }

  Encrypt.Encrypter getCipher() {
    String privateKey = this.privateKey;
    String publicKey = this.publicKey;

    final keyPair =
        KeyPair.createKeyPairFromPrivateKeyString(privateKey, SignSchema.SHA3);
    final pk = ConverterUtil.hexToUint8Array(publicKey);
    final key = KeyPair.deriveSharedKey(keyPair, pk, salt, SignSchema.SHA3);
    final Encrypt.Key keyEnc = Encrypt.Key(key);

    final encrypter =
        Encrypt.Encrypter(Encrypt.AES(keyEnc, mode: Encrypt.AESMode.cbc));
    return encrypter;
    // return crypto.createCipheriv('aes-256-cbc', key, iv);
  }
}

class NemKeysDecipher {
  static const SaltLength = 32;
  static const IvLength = 16;

  String privateKey;
  String publicKey;
  List<int> salt;
  Encrypt.IV iv;

  int saltBytesRead;
  int ivBytesRead;

  NemKeysDecipher(this.privateKey, this.publicKey) {
    this.saltBytesRead = 0;
    this.ivBytesRead = 0;
    // this.salt = Buffer.alloc(NemKeysDecipherStream.SaltLength);
    // this.iv = Buffer.alloc(NemKeysDecipherStream.IvLength);
  }

  getMeta(Uint8List encryptedData) {
    this.salt = encryptedData.sublist(0, 32);
    final iv = encryptedData.sublist(32, 32 + 16);
    this.iv = Encrypt.IV(iv);
  }

  // public _transform(chunk: any, _: string, callback: TransformCallback): void {
  //   let chunkLength = chunk.length;
  //   let chunkOffset = 0;

  //   if (this.saltBytesRead < NemKeysDecipherStream.SaltLength) {
  //     const remainingSaltBytes =
  //       NemKeysDecipherStream.SaltLength - this.saltBytesRead;
  //     chunkOffset =
  //       chunkLength <= remainingSaltBytes ? chunkLength : remainingSaltBytes;
  //     chunk.copy(this.salt, this.saltBytesRead, 0, chunkOffset);
  //     chunk = chunk.slice(chunkOffset);
  //     chunkLength = chunk.length;
  //     this.saltBytesRead += chunkOffset;
  //   }

  //   if (this.ivBytesRead < NemKeysDecipherStream.IvLength) {
  //     const remainingIvBytes =
  //       NemKeysDecipherStream.IvLength - this.ivBytesRead;
  //     chunkOffset =
  //       chunkLength <= remainingIvBytes ? chunkLength : remainingIvBytes;
  //     chunk.copy(this.iv, this.ivBytesRead, 0, chunkOffset);
  //     chunk = chunk.slice(chunkOffset);
  //     chunkLength = chunk.length;
  //     this.ivBytesRead += chunkOffset;
  //   }

  //   if (
  //     this.saltBytesRead === NemKeysDecipherStream.SaltLength &&
  //     this.ivBytesRead === NemKeysDecipherStream.IvLength &&
  //     !this.decipher
  //   ) {

  //     this.decipher = this.getDecipher(
  //       this.privateKey,
  //       this.publicKey,
  //       this.salt,
  //       this.iv
  //     );
  //   }

  //   if (this.decipher) {
  //     this.push(this.decipher.update(chunk));
  //   }
  //   callback();
  // }

  // public _flush(callback: TransformCallback): void {
  //   try {
  //     if (this.decipher) {
  //       this.push(this.decipher.final());
  //     }
  //   } catch (e) {
  //     return callback(e);
  //   }
  //   callback();
  // }

  Encrypt.Encrypter getDecipher() {
    String privateKey = this.privateKey;
    String publicKey = this.publicKey;

    final keyPair =
        KeyPair.createKeyPairFromPrivateKeyString(privateKey, SignSchema.SHA3);
    final pk = ConverterUtil.hexToUint8Array(publicKey);
    final key = KeyPair.deriveSharedKey(keyPair, pk, salt, SignSchema.SHA3);
    final Encrypt.Key keyEnc = Encrypt.Key(key);

    final encrypter =
        Encrypt.Encrypter(Encrypt.AES(keyEnc, mode: Encrypt.AESMode.cbc));
    return encrypter;
    // return crypto.createDecipheriv('aes-256-cbc', key, iv);
  }
}
