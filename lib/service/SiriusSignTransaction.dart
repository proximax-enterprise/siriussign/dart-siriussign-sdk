part of siriussign_sdk;

class SiriusSignTransaction {
  String appID;
  Account userAccount;
  String networkGenerationHash;
  int networkType;

  SiriusSignTransaction(this.appID, this.userAccount,
      this.networkGenerationHash, this.networkType);

  /// Create account restriction transaction to allow signers and verifiers to send transaction to document account
  SignedTransaction createRestriction(SiriusSignDocument document) {
    List<SignerInfo> signers = [
      document.info.owner,
      ...document.info.cosigners,
      ...document.info.verifiers
    ];
    List<AccountPropertiesAddressModification> restrictionMods =
        signers.map((signer) {
      return AccountPropertiesAddressModification(
          PropertyModificationType.addProperty,
          Address.fromRawAddress(signer.address));
    }).toList();
    AccountPropertiesAddressTransaction accountRestrictionTransaction =
        AccountPropertiesAddressTransaction(
      new Deadline(hours: 2),
      AccountPropertyType.allowAddress,
      restrictionMods,
      this.networkType,
      // Uint64.fromBigInt(new BigInt.from(0))
    );
    SignedTransaction signedAccountRestrictionTransaction = document
        .documentAccount
        .sign(accountRestrictionTransaction, this.networkGenerationHash);
    return signedAccountRestrictionTransaction;
  }

  /// Create SIGN transaction to sign a document
  SignedTransaction createDocSigningTransaction(SiriusSignDocument document,
      [List<Mosaic> mosaics = const [], Map<String, dynamic> meta]) {
    bool isOwner = document.info.owner.publicKey == this.userAccount.publicKey;
    SiriusSignDocumentSigningMessage ssMessage =
        new SiriusSignDocumentSigningMessage(
            this.appID,
            document.fileHash,
            document.fileName,
            document.fileType,
            isOwner,
            document.info.uploadTxHash,
            document.info.encryptType,
            document.signatureUploadTxHash,
            meta);

    PlainMessage message = ssMessage.toPlainMessage();

    TransferTransaction docSigningTransaction = TransferTransaction(
      new Deadline(hours: 2),
      document.info.documentAccount.address,
      mosaics,
      message,
      this.networkType,
      // Uint64.fromBigInt(new BigInt.from(0))
    );

    String networkGenerationHash = this.networkGenerationHash;
    SignedTransaction signedDocSigningTransaction =
        this.userAccount.sign(docSigningTransaction, networkGenerationHash);

    return signedDocSigningTransaction;
  }

  /// Create aggregate complete transaction that contains NOTIFY transfer transaction to cosigners and verifiers
  /// @param document
  /// @param signersSignaturesInfo
  SignedTransaction createMultiSigningNotificationTransaction(
      SiriusSignDocument document, List<Mosaic> mosaics,
      [List<Map<String, dynamic>> metaArray]) {
    // Create cosign notify
    List<SiriusSignCosignerNotifyingMessage> ssCosignMessages =
        document.info.cosigners.asMap().entries.map((entry) {
      int index = entry.key;
      SignerInfo cosigner = entry.value;
      return new SiriusSignCosignerNotifyingMessage(
          this.appID,
          document.info.uploadTxHash,
          metaArray != null ? metaArray[index] : null);
    }).toList();

    List<PlainMessage> messages = ssCosignMessages.map((ssMessage) {
      return ssMessage.toPlainMessage();
    }).toList();

    List<TransferTransaction> aggCosignNotificationTransactions =
        messages.asMap().entries.map((entry) {
      int index = entry.key;
      PlainMessage message = entry.value;
      String cosignerPublicKey = document.info.cosigners[index].publicKey;
      TransferTransaction tftx = TransferTransaction(
          new Deadline(hours: 2),
          Address.fromPublicKey(cosignerPublicKey, this.networkType),
          mosaics,
          message,
          this.networkType);
      // Uint64.fromBigInt(new BigInt.from(0))
      tftx.toAggregate = document.documentAccount.publicAccount;
      return tftx;
    }).toList();

    // Create verify notify
    SiriusSignVerifierNotifyingMessage ssVerifyMessage =
        new SiriusSignVerifierNotifyingMessage(
      this.appID,
      document.info.uploadTxHash,
    );

    PlainMessage verifyMessage = ssVerifyMessage.toPlainMessage();

    List<TransferTransaction> aggVerifyNotificationTransactions =
        document.info.verifiers.map((verifier) {
      TransferTransaction tftx = TransferTransaction(
        new Deadline(hours: 2),
        Address.fromPublicKey(verifier.publicKey, this.networkType),
        mosaics,
        verifyMessage,
        this.networkType,
        // Uint64.fromBigInt(new BigInt.from(0))
      );
      tftx.toAggregate = document.documentAccount.publicAccount;
      return tftx;
    }).toList();

    // Notify transactions
    List<TransferTransaction> aggNotificationTransactions = [
      ...aggCosignNotificationTransactions,
      ...aggVerifyNotificationTransactions
    ];
    AggregateTransaction aggTransaction = AggregateTransaction.complete(
      new Deadline(hours: 2),
      aggNotificationTransactions,
      this.networkType,
      // [],
      // Uint64.fromBigInt(new BigInt.from(0))
    );

    String networkGenerationHash = this.networkGenerationHash;
    SignedTransaction signedAggTransaction =
        document.documentAccount.sign(aggTransaction, networkGenerationHash);
    return signedAggTransaction;
  }

  /// Create VERIFY transaction to sign a document
  SignedTransaction createDocVerifyingTransaction(SiriusSignDocument document,
      [Map<String, dynamic> meta]) {
    bool isOwner = document.info.owner.publicKey == this.userAccount.publicKey;
    SiriusSignDocumentVerifyingMessage ssMessage =
        new SiriusSignDocumentVerifyingMessage(
            this.appID,
            document.fileHash,
            document.fileName,
            document.fileType,
            isOwner,
            document.info.uploadTxHash,
            document.info.encryptType,
            meta);

    PlainMessage message = ssMessage.toPlainMessage();

    TransferTransaction docVerifyingTransaction = TransferTransaction(
      new Deadline(hours: 2),
      document.info.documentAccount.address,
      [],
      message,
      this.networkType,
      // Uint64.fromBigInt(new BigInt.from(0))
    );

    String networkGenerationHash = this.networkGenerationHash;
    SignedTransaction signedDocVerifyingTransaction =
        this.userAccount.sign(docVerifyingTransaction, networkGenerationHash);

    return signedDocVerifyingTransaction;
  }
}
