part of siriussign_sdk.ipfs;

class DownloadResultData {
  IpfsClient _ipfsClient;
  PrivacyStrategy _privacyStrategy;
  String dataHash;
  int timestamp;
  String digest;
  String description;
  String contentType;
  String name;
  Map<String, String> metadata;

  DownloadResultData(
    this._ipfsClient,
    this._privacyStrategy,
    this.dataHash,
    this.timestamp, [
    this.digest,
    this.description,
    this.contentType,
    this.name,
    this.metadata,
  ]);

  Future<Uint8List> getContentAsBytes() async {
    final data = await this._ipfsClient.getData(this.dataHash);
    return this._privacyStrategy.decryptList(data);
    // final data = await this._ipfsClient.getDataStream(this.dataHash);
    // final downloadedData = this._privacyStrategy.decrypt(Stream.castFrom(data));
    // return Uint8List.fromList(downloadedData.toList());
  }

  Future<String> getContentAsString() async {
    final data = await this.getContentAsBytes();
    return utf8.decode(data);
  }
}
