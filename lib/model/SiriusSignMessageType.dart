part of siriussign_sdk;

/// Sirius Sign Message Type
class SiriusSignMessageType {
  static const SIGN = 1;
  static const SIGN_NOTIFY = 2;
  static const VERIFY = 3;
  static const VERIFY_NOTIFY = 4;
}
