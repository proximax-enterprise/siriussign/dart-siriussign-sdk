part of siriussign_sdk.ipfs;

class UploadParameter {
  UploadParameterData data;
  String signerPrivateKey;
  PrivacyStrategy privacyStrategy;
  int transactionDeadline;
  bool useBlockchainSecureMessage;
  String generateHas;
  bool detectContentType;
  bool computeDigest;
  List<Mosaic> transactionMosaics;
  String recipientPublicKey;
  String recipientAddress;

  UploadParameter(
    this.data,
    this.signerPrivateKey,
    this.privacyStrategy,
    this.transactionDeadline,
    this.useBlockchainSecureMessage,
    this.generateHas,
    this.detectContentType,
    this.computeDigest, [
    this.transactionMosaics,
    this.recipientPublicKey,
    this.recipientAddress,
  ]);
}
