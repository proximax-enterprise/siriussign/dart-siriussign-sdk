part of siriussign_sdk;

class NodeUrl {
  String protocol;
  String domain;
  int port;

  NodeUrl(this.protocol,  this.domain,  this.port,);
}

class NodeUtil {
    static NodeUrl split(String node) {
        String protocol = node.split('://')[0];
        String domain = node.split('//')[1].split(':')[0];
        String portString = node.split('//')[1].split(':')[1];
        int port = (portString != null) ? int.parse(portString, radix: 10) : null;
        return NodeUrl(domain, protocol, port);
    }

    static String getWebsocketAddress(String apiNode) {
        return apiNode.replaceAll('https', 'wss').replaceAll('http', 'ws');
    }

    static Future<String> fetchNetworkGenerationHash(String apiNode) async {
      final client = SiriusClient.fromUrl(apiNode, null);
      String generationHash = await client.generationHash;
      return generationHash;
    }

    static Future<int> fetchNetworkType(String apiNode) async {
      final client = SiriusClient.fromUrl(apiNode, null);
      int networkType = await client.networkType;
      return networkType;
    }
}