part of siriussign_sdk.ipfs;

class PrivacyType {
  static const int PLAIN = 1001;
  static const int NEM_KEYS = 1002;
  static const int SHAMIR = 1003;
  static const int PASSWORD = 1004;
  static const int CUSTOM = 2001;
}

abstract class PrivacyStrategy {
  /// Get the privacy type's int value
  /// @return the privacy type's int value
  int getPrivacyType();

  Stream<List<int>> encrypt(Stream plainData);

  Stream<List<int>> decrypt(Stream encryptedData);

  Uint8List encryptList(Uint8List plainData);

  Uint8List decryptList(Uint8List encryptedData);
}

class PlainPrivacyStrategy implements PrivacyStrategy {
  PlainPrivacyStrategy();

  static create() {
    return new PlainPrivacyStrategy();
  }

  int getPrivacyType() {
    return PrivacyType.PLAIN;
  }

  Stream<List<int>> encrypt(Stream plainData) {
    return plainData;
  }

  Stream<List<int>> decrypt(Stream encryptedData) {
    return encryptedData;
  }

  Uint8List encryptList(Uint8List plainData) {
    return plainData;
  }

  Uint8List decryptList(Uint8List encryptedData) {
    return encryptedData;
  }
}

/// The privacy strategy that secures data using the NEM keys (a private key and a public key).
/// This strategy encrypt and decrypt the data using both private and public keys
class NemPrivacyStrategy implements PrivacyStrategy {
  String privateKey;
  String publicKey;

  /// Create instance of this strategy
  /// @param privateKey the private key
  /// @param publicKey the public key
  /// @return the instance of this strategy
  static create(String privateKey, String publicKey) {
    return new NemPrivacyStrategy(privateKey, publicKey);
  }

  NemPrivacyStrategy(this.privateKey, this.publicKey);

  /// Get the privacy type which is set as NEMKEYS
  /// @return the privacy type's int value
  /// @see PrivacyType
  int getPrivacyType() {
    return PrivacyType.NEM_KEYS;
  }

  Stream<List<int>> encrypt(Stream plainData) {}

  Stream<List<int>> decrypt(Stream plainData) {}

  /// Encrypt byte data using the private and public keys provided
  /// @param data the byte data to encrypt
  /// @return the encrypted byte data
  Uint8List encryptList(Uint8List plainData) {
    final cipher = new NemKeysCipher(this.privateKey, this.publicKey);
    final encrypter = cipher.getCipher();
    final cipherData = encrypter.encryptBytes(plainData, iv: cipher.iv).bytes;
    final encryptedData =
        Uint8List.fromList([...cipher.salt, ...cipher.iv.bytes, ...cipherData]);
    return encryptedData;
  }

  /// Encrypt byte data using the private and public keys provided
  /// @param encryptedStream the byte data to decrypt
  /// @return the decrypted byte data
  Uint8List decryptList(Uint8List encryptedData) {
    final cipher = new NemKeysDecipher(this.privateKey, this.publicKey);
    cipher.getMeta(encryptedData);
    final encrypter = cipher.getDecipher();
    final downloadRes = encrypter.decryptBytes(
        Encrypt.Encrypted(encryptedData.sublist(32 + 16)),
        iv: cipher.iv);
    return Uint8List.fromList(downloadRes);
  }
}

class PasswordPrivacyStrategy implements PrivacyStrategy {
  String _password;

  /// Create instance of this strategy
  static create(String password) {
    return new PasswordPrivacyStrategy(password);
  }

  PasswordPrivacyStrategy(this._password);

  /// Get the privacy type which is set as PASSWORD
  /// @return the privacy type's int value
  /// @see PrivacyType
  int getPrivacyType() {
    return PrivacyType.PASSWORD;
  }

  Stream<List<int>> encrypt(Stream plainData) {}

  Stream<List<int>> decrypt(Stream plainData) {}

  /// Encrypt byte data using the password provided
  /// @param data the byte data to encrypt
  /// @return the encrypted byte data
  Uint8List encryptList(Uint8List plainData) {
    final cipher = new PasswordCipher(this._password);
    final encrypter = cipher.getCipher();
    final cipherData = encrypter.encryptBytes(plainData, iv: cipher.iv).bytes;
    final encryptedData =
        Uint8List.fromList([...cipher.salt, ...cipher.iv.bytes, ...cipherData]);
    return encryptedData;
  }

  /// Encrypt byte data using the private and public keys provided
  /// @param encryptedStream the byte data to decrypt
  /// @return the decrypted byte data
  Uint8List decryptList(Uint8List encryptedData) {
    final cipher = new PasswordDecipher(this._password);
    cipher.getMeta(encryptedData);
    final encrypter = cipher.getDecipher();
    final downloadRes = encrypter.decryptBytes(
        Encrypt.Encrypted(encryptedData.sublist(32 + 16)),
        iv: cipher.iv);
    return Uint8List.fromList(downloadRes);
  }
}
