part of siriussign_sdk.ipfs;

class PasswordCipher {
  static const _SaltLength = 32;
  static const _IvLength = 16;
  Encrypt.Encrypter cipher;
  String _password;
  Encrypt.IV iv;
  List<int> salt = List<int>.filled(32, 0);

  PasswordCipher(this._password) {
    // final salt = crypto.randomBytes(32);

    this.iv = Encrypt.IV.fromSecureRandom(PasswordCipher._IvLength);
    this.cipher = this.getCipher();
  }

  Encrypt.Encrypter getCipher() {
    final generator = new PBKDF2(hashAlgorithm: sha256);
    final salt = Salt.generateAsBase64String(PasswordCipher._SaltLength);
    this.salt = ascii.encode(salt);
    final key = generator.generateKey(this._password, salt, 65536, 32);
    final Encrypt.Key keyEnc = Encrypt.Key(key);

    final encrypter =
        Encrypt.Encrypter(Encrypt.AES(keyEnc, mode: Encrypt.AESMode.cbc));
    return encrypter;
  }
}

class PasswordDecipher {
  String _password;
  List<int> _salt;
  Encrypt.IV iv;

  PasswordDecipher(this._password);

  getMeta(Uint8List encryptedData) {
    this._salt = encryptedData.sublist(0, 32);
    final iv = encryptedData.sublist(32, 32 + 16);
    this.iv = Encrypt.IV(iv);
  }

  Encrypt.Encrypter getDecipher() {
    final generator = new PBKDF2(hashAlgorithm: sha256);
    final salt = ascii.decode(this._salt);
    final key = generator.generateKey(this._password, salt, 65536, 32);
    final Encrypt.Key keyEnc = Encrypt.Key(key);

    final encrypter =
        Encrypt.Encrypter(Encrypt.AES(keyEnc, mode: Encrypt.AESMode.cbc));
    return encrypter;
  }
}
