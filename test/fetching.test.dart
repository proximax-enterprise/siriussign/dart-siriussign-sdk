import 'package:flutter_test/flutter_test.dart';
import 'package:siriussign_sdk/siriussign_sdk.dart';
import 'package:xpx_chain_sdk/xpx_sdk.dart';
import 'constant.test.dart';

void main() {
  group('Document fetch tests', () {
    test('should get all completed, waiting docs info from owner', () async {
      final ownerAcc =
          Account.fromPrivateKey(PRIVATE_KEY['privateTest_1'], NETWORK_TYPE);
      final documentFetch = new DocumentFetch(APP_ID, API_NODE, NETWORK_TYPE);
      final allDocs =
          await documentFetch.allCompletedAndWaiting(ownerAcc.publicAccount);
      final completed = allDocs['completed'];
      final waiting = allDocs['waiting'];
      final verified = allDocs['verified'];
      final verifying = allDocs['verifying'];

      expect(completed.length > 1, true);
      completed.forEach((doc) {
        expect(doc.isCompleted, true);
        expect(doc.owner.address, ownerAcc.address.address);
        expect(doc.owner.publicKey, ownerAcc.publicKey);
        expect(doc.owner.isSigned, true);
      });

      expect(waiting.length > 1, true);
      waiting.forEach((doc) {
        expect(doc.isCompleted, false);
        expect(doc.owner.address, ownerAcc.address.address);
        expect(doc.owner.publicKey, ownerAcc.publicKey);
        expect(doc.owner.isSigned, true);
      });

      expect(verified.length, 0);
      expect(verifying.length, 0);
    }, timeout: Timeout.parse('90000ms'));

    test('should get all completed, waiting docs info from co-signer',
        () async {
      final ownerAcc =
          Account.fromPrivateKey(PRIVATE_KEY['privateTest_1'], NETWORK_TYPE);
      final cosignerAcc =
          Account.fromPrivateKey(PRIVATE_KEY['privateTest_2'], NETWORK_TYPE);
      final documentFetch = new DocumentFetch(APP_ID, API_NODE, NETWORK_TYPE);
      final allDocs =
          await documentFetch.allCompletedAndWaiting(cosignerAcc.publicAccount);
      final completed = allDocs['completed'];
      final waiting = allDocs['waiting'];
      final verified = allDocs['verified'];
      final verifying = allDocs['verifying'];

      expect(completed.length > 1, true);
      completed.forEach((doc) {
        expect(doc.isCompleted, true);
        expect(doc.owner.address, ownerAcc.address.address);
        expect(doc.owner.publicKey, ownerAcc.publicKey);
        expect(doc.owner.isSigned, true);

        expect(doc.cosigners[0].address, cosignerAcc.address.address);
        expect(doc.cosigners[0].publicKey, cosignerAcc.publicKey);
        expect(doc.cosigners[0].isSigned, true);
      });

      expect(waiting.length, 0);
      expect(verified.length, 0);
      expect(verifying.length, 0);
    }, timeout: Timeout.parse('60000ms'));

    test('should get all needsign docs info from co-signer', () async {
      final ownerAcc =
          Account.fromPrivateKey(PRIVATE_KEY['privateTest_1'], NETWORK_TYPE);
      final cosignerAcc =
          Account.fromPrivateKey(PRIVATE_KEY['privateTest_2'], NETWORK_TYPE);
      final documentFetch = new DocumentFetch(APP_ID, API_NODE, NETWORK_TYPE);

      final allNeedDocs =
          await documentFetch.allNeedSignVerify(cosignerAcc.publicAccount);
      final needSign = allNeedDocs['needSign'];
      final needVerify = allNeedDocs['needVerify'];

      expect(needSign.length > 1, true);
      needSign.forEach((doc) {
        expect(doc.isCompleted, false);
        expect(doc.owner.address, ownerAcc.address.address);
        expect(doc.owner.publicKey, ownerAcc.publicKey);
        expect(doc.owner.isSigned, true);

        expect(doc.cosigners[0].address, cosignerAcc.address.address);
        expect(doc.cosigners[0].publicKey, cosignerAcc.publicKey);
        expect(doc.cosigners[0].isSigned, false);
      });

      expect(needVerify.length, 0);
    }, timeout: Timeout.parse('120000ms'));

    test(
        'should get completed, waiting docs info from owner (by transaction page)',
        () async {
      final ownerAcc =
          Account.fromPrivateKey(PRIVATE_KEY['privateTest_1'], NETWORK_TYPE);
      final documentFetch = new DocumentFetch(APP_ID, API_NODE, NETWORK_TYPE);

      final docs =
          await documentFetch.completedAndWaiting(ownerAcc.publicAccount, null);
      final completed = docs['completed'];
      final waiting = docs['waiting'];
      final verified = docs['verified'];
      final verifying = docs['verifying'];
      final nextHash = docs['nextHash'];

      expect(completed.length > 0, true);
      completed.forEach((doc) {
        expect(doc.isCompleted, true);
        expect(doc.owner.address, ownerAcc.address.address);
        expect(doc.owner.publicKey, ownerAcc.publicKey);
        expect(doc.owner.isSigned, true);
      });

      expect(verified.length, 0);
      expect(verifying.length, 0);
    }, timeout: Timeout.parse('60000ms'));

    test(
        'should get all completed, waiting docs info from owner (by transaction page)',
        () async {
      final ownerAcc =
          Account.fromPrivateKey(PRIVATE_KEY['privateTest_1'], NETWORK_TYPE);
      final documentFetch = new DocumentFetch(APP_ID, API_NODE, NETWORK_TYPE);

      var fetchRes =
          await documentFetch.completedAndWaiting(ownerAcc.publicAccount, null);
      var firstHash = null;
      var lastHash = fetchRes['nextHash'];
      List<SiriusSignDocumentInfo> completed = fetchRes['completed'],
          waiting = fetchRes['waiting'],
          verified = fetchRes['verified'],
          verifying = fetchRes['verifying'];
      while (firstHash != lastHash && lastHash != null) {
        fetchRes = await documentFetch.completedAndWaiting(
            ownerAcc.publicAccount, lastHash);
        completed = [...completed, ...fetchRes['completed']];
        waiting = [...waiting, ...fetchRes['waiting']];
        verified = [...verified, ...fetchRes['verified']];
        verifying = [...verifying, ...fetchRes['verifying']];
        firstHash = lastHash;
        lastHash = fetchRes['nextHash'];
      }

      final allRes =
          await documentFetch.allCompletedAndWaiting(ownerAcc.publicAccount);

      allRes['completed'].forEach((doc) {
        expect(completed.map((e) => e.id).toList().contains(doc.id), true);
      });

      allRes['waiting'].forEach((doc) {
        expect(waiting.map((e) => e.id).toList().contains(doc.id), true);
      });

      allRes['verified'].forEach((doc) {
        expect(verified.map((e) => e.id).toList().contains(doc.id), true);
      });

      allRes['verifying'].forEach((doc) {
        expect(verifying.map((e) => e.id).toList().contains(doc.id), true);
      });
    }, timeout: Timeout.parse('120000ms'));

    test('should get needsign docs info from co-signer (by transaction page)',
        () async {
      final ownerAcc =
          Account.fromPrivateKey(PRIVATE_KEY['privateTest_1'], NETWORK_TYPE);
      final cosignerAcc =
          Account.fromPrivateKey(PRIVATE_KEY['privateTest_2'], NETWORK_TYPE);
      final documentFetch =
          new DocumentFetch(APP_ID, API_NODE, NETWORK_TYPE, 99);

      final needDocs =
          await documentFetch.needSignVerify(cosignerAcc.publicAccount, null);
      final needSign = needDocs['needSign'];
      final needVerify = needDocs['needVerify'];
      final nextHash = needDocs['nextHash'];

      expect(needSign.length > 1, true);
      needSign.forEach((doc) {
        expect(doc.isCompleted, false);
        expect(doc.owner.address, ownerAcc.address.address);
        expect(doc.owner.publicKey, ownerAcc.publicKey);
        expect(doc.owner.isSigned, true);

        expect(doc.cosigners[0].address, cosignerAcc.address.address);
        expect(doc.cosigners[0].publicKey, cosignerAcc.publicKey);
        expect(doc.cosigners[0].isSigned, false);
      });

      expect(needVerify.length, 0);
    }, timeout: Timeout.parse('60000ms'));
  });
}
