part of siriussign_sdk.ipfs;

class SignSchema {
  static const KECCAK_REVERSED_KEY = 1;
  static const SHA3 = 2;
}
