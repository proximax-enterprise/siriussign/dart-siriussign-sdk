import 'package:xpx_chain_sdk/xpx_sdk.dart';

const APP_ID = 'AppName';
const API_NODE = 'https://demo-sc-api-1.ssi.xpxsirius.io';
const IPFS_NODE = 'https://ipfs1-dev.xpxsirius.io:5443';
const NETWORK_TYPE = privateTest;
const PRIVATE_KEY = {
  "privateTest_1":
      'DAC997C88B400CB8509BBA1E4F4D635340758FFCA165F0BBEB3E5C839844162A',
  "privateTest_2":
      '12DDAAED8ED1081AA0942351121F6C62A387588897F6857FB0A7521734E528DA'
};
