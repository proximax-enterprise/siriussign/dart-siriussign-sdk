part of siriussign_sdk;

class SiriusSignDocumentVerifyingMessage extends SiriusSignMessage {
  Map<String, dynamic> body = {
    "fileHash": String,
    "fileName": String,
    "fileType": String,
    "isOwner": bool,
    "uploadTxHash": String,
    "encryptType": int,
  };

  Map<String, dynamic> meta;

  SiriusSignDocumentVerifyingMessage(
      String appID,
      String fileHash,
      String fileName,
      String fileType,
      bool isOwner,
      String uploadTxHash,
      int encryptType,
      [Map<String, dynamic> meta])
      : super(appID, SiriusSignMessageType.VERIFY) {
    this.body = {
      "fileHash": fileHash,
      "fileName": fileName,
      "fileType": fileType,
      "isOwner": isOwner,
      "uploadTxHash": uploadTxHash,
      "encryptType": encryptType,
    };
    this.meta = meta != null ? meta : {};
  }

  static SiriusSignDocumentVerifyingMessage createFromPayload(String payload) {
    Map<String, dynamic> message;
    try {
      message = jsonDecode(payload);
    } catch (e) {
      throw ('M01: Payload is not a Sirius Sign message string');
    }
    if (message['header']['application'] != APPLICATION)
      throw ('M01: Payload is not a Sirius Sign message string');
    if (message['header']['messageType'] != SiriusSignMessageType.VERIFY)
      throw ('M04: This Sirius Sign Message is not a Document Verifying Message');

    return new SiriusSignDocumentVerifyingMessage(
        message['header']['appID'],
        message['body']['fileHash'],
        message['body']['fileName'],
        message['body']['fileType'],
        message['body']['isOwner'],
        message['body']['uploadTxHash'],
        message['body']['encryptType'],
        message['meta']);
  }
}
