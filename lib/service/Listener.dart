part of siriussign_sdk;

class ListenerChannelName {
  static const block = 'block';
  static const confirmedAdded = 'confirmedAdded';
  static const unconfirmedAdded = 'unconfirmedAdded';
  static const unconfirmedRemoved = 'unconfirmedRemoved';
  static const aggregateBondedAdded = 'partialAdded';
  static const aggregateBondedRemoved = 'partialRemoved';
  static const cosignature = 'cosignature';
  static const modifyMultisigAccount = 'modifyMultisigAccount';
  static const status = 'status';
}

class ListenerResponse {
  String channel;
  dynamic message;
  ListenerResponse(this.channel, this.message);
}

class Listener {
  String url;
  String _websocketUrl;
  WebSocket websocket;
  String _uid;
  ListenerResponse _listenerResponse;

  Listener(this.url) {
    this._websocketUrl =
        this.url.replaceAll('https', 'wss').replaceAll('http', 'ws') + '/ws';
  }

  parseTransaction(Map<String, dynamic> tx) {
    const int aggregateCompleted = 0x4141;
    const int aggregateBonded = 0x4241;
    const int addressAlias = 0x424E;
    const int metadataAddress = 0x413d;
    const int metadataMosaic = 0x423d;
    const int metadataNamespace = 0x433d;
    const int mosaicDefinition = 0x414d;
    const int mosaicAlias = 0x434e;
    const int mosaicSupplyChange = 0x424d;
    const int modifyMultisig = 0x4155;
    const int modifyContract = 0x4157;
    const int registerNamespace = 0x414e;
    const int transfer = 0x4154;
    const int lock = 0x4148;
    const int secretLock = 0x4152;
    const int secretProof = 0x4252;
    const int addExchangeOffer = 0x415D;
    const int exchangeOffer = 0x425D;
    const int removeExchangeOffer = 0x435D;
    const int accountPropertyAddress = 0x4150;
    const int accountPropertyMosaic = 0x4250;
    const int accountPropertyEntityType = 0x4350;

    switch (tx['transaction']['type']) {
      case transfer:
        {
          return TransferTransaction.fromDTO(
              TransferTransactionInfoDTO.fromJson(tx));
        }
      case aggregateBonded:
      case aggregateCompleted:
        {
          final meta = tx['meta'];
          final reducedInnerTxs = tx['transaction']['transactions'];
          final fullInnerTxs = (reducedInnerTxs as List)
              .map((rTx) => {'transaction': rTx['transaction'], 'meta': meta})
              .toList();
          final aggTx = tx;
          aggTx['transaction']['transactions'] = fullInnerTxs;
          return AggregateTransaction.fromDTO(
              AggregateTransactionInfoDTO.fromJson(aggTx));
        }
      case accountPropertyAddress:
        {
          return AccountPropertiesAddressTransaction.fromDTO(
              AccountPropertiesAddressTransactionInfoDTO.fromJson(tx));
        }
      default:
        throw ('Get an unsupprted transaction type');
    }
  }

  Future<Listener> open() {
    Completer completer = new Completer();
    WebSocket.connect(this._websocketUrl).then((WebSocket ws) {
      this.websocket = ws;
      ws.listen(
        (event) {
          final res = json.decode(event);
          if (res['uid'] != null) {
            this._uid = res['uid'];
            completer.complete(this);
          } else if (res['transaction'] != null) {
            final tx = this.parseTransaction(res);
            this._listenerResponse =
                ListenerResponse(res["meta"]["channelName"], tx);
          } else if (res['status'] != null) {
            final status = TransactionStatus.fromJson(res);
            this._listenerResponse =
                ListenerResponse(ListenerChannelName.status, status);
          }
        },
        onDone: () {},
        onError: (err) => print('Error: ${err.toString()}'),
        cancelOnError: true,
      );
    }, onError: (err) => print('Error: ${err.toString()}'));
    return completer.future.then((value) => value);
  }

  close() {
    return this.websocket.close();
  }

  _flushListenerResponse() {
    this._listenerResponse = null;
  }

  Future<dynamic> confirmed(Address address) {
    Completer completer = new Completer();
    this._flushListenerResponse();
    if (this.websocket.readyState != 1)
      completer.completeError('Websocket is closed');
    else {
      this.websocket.add(json.encode({
            'uid': this._uid,
            'subscribe': 'confirmedAdded/' + address.address
          }));
      Timer.periodic(Duration(milliseconds: 10), (timer) {
        if ((this._listenerResponse != null) &&
            (this._listenerResponse.channel ==
                ListenerChannelName.confirmedAdded)) {
          completer.complete(this._listenerResponse.message);
          timer.cancel();
        }
        if (this.websocket.readyState != 1 && (!completer.isCompleted)) {
          completer.complete('Not a confirmed transaction');
          timer.cancel();
        }
      });
    }
    return completer.future.then((value) => value);
  }

  Future<dynamic> status(Address address) {
    Completer completer = new Completer();
    this._flushListenerResponse();
    if (this.websocket.readyState != 1)
      completer.completeError('Websocket is closed');
    else {
      this.websocket.add(json.encode(
          {'uid': this._uid, 'subscribe': 'status/' + address.address}));
      Timer.periodic(Duration(milliseconds: 10), (timer) {
        if ((this._listenerResponse != null) &&
            (this._listenerResponse.channel == ListenerChannelName.status)) {
          completer.complete(this._listenerResponse.message);
          timer.cancel();
        }
        if (this.websocket.readyState != 1 && (!completer.isCompleted)) {
          completer.completeError('Not a status');
          timer.cancel();
        }
      });
    }
    return completer.future.then((value) => value);
  }
}
