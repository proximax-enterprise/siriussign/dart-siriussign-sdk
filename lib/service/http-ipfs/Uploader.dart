part of siriussign_sdk.ipfs;

class Uploader {
  IpfsClient _ipfsClient;
  SiriusClient _siriusClient;
  TransactionListener _listener;

  Uploader(String chainNode, String ipfsNode) {
    this._ipfsClient = IpfsClient.fromUri(ipfsNode);
    this._siriusClient = SiriusClient.fromUrl(chainNode, null);
    this._listener = TransactionListener(chainNode);
  }

  Future<SignedTransaction> upload(UploadParameter param) async {
    final privacyStrategyToUse = param.privacyStrategy;
    // final res = await this._ipfsClient.addStream(
    //     privacyStrategyToUse.encrypt(param.data.getByteStream()),
    //     param.data.data.length);
    final res = await this
        ._ipfsClient
        .addBytes(privacyStrategyToUse.encryptList(param.data.data));
    final ipfsDataHash = res.hash;
    final uploadTx = this._buildTransaction(param, ipfsDataHash);
    final recipientAddress = Address.fromRawAddress(param.recipientAddress);
    final networkType = recipientAddress.networkType;
    final signerAccount =
        Account.fromPrivateKey(param.signerPrivateKey, networkType);
    final signedUploadTx = signerAccount.sign(uploadTx, param.generateHas);
    await this._listener.waitForTransactionConfirmed(
        signerAccount.address, signedUploadTx.hash, () {
      this._siriusClient.transaction.announce(signedUploadTx);
    });
    return signedUploadTx;
  }

  _buildTransaction(UploadParameter param, String ipfsDataHash) {
    final message = UploadMessage(
            param.privacyStrategy.getPrivacyType(),
            param.data.contentType,
            ipfsDataHash,
            param.data.description,
            param.data.metadata,
            param.data.name)
        .getPlainMessage();
    final recipientAddress = Address.fromRawAddress(param.recipientAddress);
    return TransferTransaction(
        Deadline(hours: param.transactionDeadline),
        recipientAddress,
        param.transactionMosaics,
        message,
        recipientAddress.networkType);
  }
}
