part of siriussign_sdk.ipfs;

class AddResponse {
  final int bytes;
  final String hash;
  final String name;
  final String size;

  AddResponse(this.bytes, this.hash, this.name, this.size);

  AddResponse.fromJson(Map<String, dynamic> json)
      : bytes = json['Bytes'],
        hash = json['Hash'],
        name = json['Name'],
        size = json['Size'];
}
