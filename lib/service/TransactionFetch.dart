part of siriussign_sdk;

typedef TransactionsFetchFn = Future<List<Transaction>> Function(
    PublicAccount publicAccount, String lastId);

class TransactionFetch {
  String apiNode;
  int networkType;
  int queryPageSize = 10;
  SiriusClient client;

  TransactionFetch(this.apiNode, this.networkType, [this.queryPageSize = 10]) {
    this.client = SiriusClient.fromUrl(this.apiNode, null);
  }

  /// Fetch transaction by hash
  /// @param hash
  Future<Transaction> fetchTransaction(String hash) {
    return this.client.transaction.getTransaction(hash);
  }

  Future<String> fetchTransactionId(String hash) async {
    if (hash == null) return null;
    Transaction tx = await this.fetchTransaction(hash);
    return tx.toJson()['abstractTransaction']['id'];
  }

  Future<List<Transaction>> fetchConfirmedTransactions(
      PublicAccount account, String lastId) {
    // console.log('fetchConfirmedTransactions: ' + lastId);
    // const querryParams =  new QueryParams(this.queryPageSize, lastId);
    return this
        .client
        .account
        .transactions(account, pageSize: this.queryPageSize, id: lastId);
  }

  Future<List<Transaction>> fetchIncomingTransactions(
      PublicAccount account, String lastId) {
    // console.log('fetchIncomingTransactions: ' + lastId);
    // const querryParams =  new QueryParams(this.queryPageSize, lastId);
    return this.client.account.incomingTransactions(account,
        pageSize: this.queryPageSize, id: lastId);
  }

  Future<List<Transaction>> fetchOutgoingTransactions(
      PublicAccount account, String lastId) {
    // console.log('fetchOutgoingTransactions: ' + lastId);
    // const querryParams =  new QueryParams(this.queryPageSize, lastId);
    return this.client.account.outgoingTransactions(account,
        pageSize: this.queryPageSize, id: lastId);
  }

  /// Fetch aggregate complete transaction of an account
  /// @param publicAccount
  Future<Map<String, dynamic>> fetchAggregateCommpleteTransactions(
      PublicAccount publicAccount, String lastId) async {
    List<Transaction> confirmedTxs =
        await this.fetchConfirmedTransactions(publicAccount, lastId);
    List<AggregateTransaction> aggCompleteTxs = confirmedTxs
        .where((tx) {
          final parsedTx = SimpleParsedTransaction.fromTransaction(tx);
          return parsedTx.type == TransactionType.aggregateCompleted.value;
        })
        .cast<AggregateTransaction>()
        .toList();
    return {
      "txs": aggCompleteTxs,
      "lastHash": SimpleParsedTransaction.fromTransaction(
              confirmedTxs[confirmedTxs.length - 1])
          .transactionHash
    };
  }

  /// Fetch all transactions of a kind of transation according to fnFetch
  /// @param publicAccount
  /// @param isReturnAll return all transactions fetched
  /// @param fnFetch
  /// @param fnEachFetch
  Future<List<Transaction>> fetchAll(
      PublicAccount publicAccount,
      bool isReturnAll,
      TransactionsFetchFn fnFetch,
      Function fnEachFetch) async {
    List<Transaction> allTxs = [];
    String lastId = null;
    bool isLast = false;
    var loadAll;
    loadAll = () async {
      if (!isLast) {
        List<Transaction> txs =
            await fnFetch(publicAccount, lastId).catchError((err) {
          return null;
        });
        if (txs != null) {
          String last;
          if (txs.length > 0)
            last =
                SimpleParsedTransaction.fromTransaction(txs[txs.length - 1]).id;
          else
            last = null;
          isLast = (txs.length < 10) || (last == lastId);
          lastId = last;

          fnEachFetch(txs);
          if (isReturnAll) allTxs = [...allTxs, ...txs];
        } else {
          isLast = true;
        }

        await Util.sleep(100);
        await loadAll();
      }
    };
    await loadAll();
    if (isReturnAll) return allTxs;
  }

  /// Fetch all incoming transations of an account
  /// @param publicAccount
  Future<List<Transaction>> fetchAllIncomingTransactions(
      PublicAccount publicAccount) async {
    List<Transaction> allIncomingTxs = await this.fetchAll(
        publicAccount,
        true,
        (PublicAccount publicAccount, String lastId) =>
            this.fetchIncomingTransactions(publicAccount, lastId),
        (List<Transaction> txs) {
      return txs;
    });
    return allIncomingTxs;
  }

  /// Fetch all outgoing transations of an account
  /// @param publicAccount
  Future<List<Transaction>> fetchAllOutgoingTransactions(
      PublicAccount publicAccount) async {
    List<Transaction> allOutgoingTxs = await this.fetchAll(
        publicAccount,
        true,
        (PublicAccount publicAccount, String lastId) =>
            this.fetchOutgoingTransactions(publicAccount, lastId),
        (List<Transaction> txs) {
      return txs;
    });
    return allOutgoingTxs;
  }

  /// Fetch all aggregate complete transaction of an account
  /// @param publicAccount
  Future<List<AggregateTransaction>> fetchAllAggregateCommpleteTransactions(
      PublicAccount publicAccount) async {
    List<AggregateTransaction> allAggCompleteTxs = [];
    await this.fetchAll(
        publicAccount,
        false,
        (PublicAccount publicAccount, String lastId) =>
            this.fetchConfirmedTransactions(publicAccount, lastId),
        (List<Transaction> txs) {
      txs.forEach((tx) {
        final parsedTx = SimpleParsedTransaction.fromTransaction(tx);
        if (parsedTx.type == TransactionType.aggregateCompleted.value)
          allAggCompleteTxs = [
            ...allAggCompleteTxs,
            tx
          ]; // <AggregateTransaction>
      });
    });
    return allAggCompleteTxs;
  }

  /// Fetch public account of address
  /// @param address
  Future<PublicAccount> fetchPublicAccount(Address address) async {
    AccountInfo docAccInfo =
        await this.client.account.getAccountInfo(address).catchError((err) {
      print(err);
      return null;
    });
    String docAccPublicKey = docAccInfo.publicKey;
    PublicAccount docPublicAcc =
        PublicAccount.fromPublicKey(docAccPublicKey, this.networkType);
    return docPublicAcc;
  }
}
