part of siriussign_sdk;

class SignerInfo {
  String publicKey;
  String address;
  bool isSigned;
  DateTime signDate;
  String signTxHash;
  String signatureUploadTxHash;

  SignerInfo(
    this.publicKey,
    this.address,
    this.isSigned,
    this.signDate,
    this.signTxHash,
    this.signatureUploadTxHash,
  );

  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = {
      "address": this.address,
      "publicKey": this.publicKey,
      "isSigned": this.isSigned,
      "signDate": this.signDate,
      "signTxHash": this.signTxHash,
      "signatureUploadTxHash": this.signatureUploadTxHash
    };
    return json;
  }
}
