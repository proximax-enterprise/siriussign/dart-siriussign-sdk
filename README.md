# dart-siriussign-sdk
SiriusSign SDK for Dart/Flutter.  
A blockchain-based document notarization solution using Sirius chain. You can easily build your own document signing application, procedure and sign digital signature on your documents and keep them safe on ProximaX blockchain and storage.
## Install
In your dart/flutter project, add this to ```pubspec.yaml```:  
```
dependencies:
  siriussign_sdk:
    path: <path to siriussign_sdk>
```