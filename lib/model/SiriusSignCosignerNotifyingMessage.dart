part of siriussign_sdk;

class SiriusSignCosignerNotifyingMessage extends SiriusSignMessage {
  Map<String, dynamic> body = {
    "uploadTxHash": String,
    // signaturePositions: SignaturePosition[];
  };

  Map<String, dynamic> meta;

  SiriusSignCosignerNotifyingMessage(String appID, String uploadTxHash,
      [Map<String, dynamic> meta])
      : super(appID, SiriusSignMessageType.SIGN_NOTIFY) {
    this.body = {
      "uploadTxHash": uploadTxHash,
    };
    this.meta = meta != null ? meta : {};
  }

  static SiriusSignCosignerNotifyingMessage createFromPayload(String payload) {
    Map<String, dynamic> message;
    try {
      message = jsonDecode(payload);
    } catch (e) {
      throw ('M01: Payload is not a Sirius Sign message string');
    }
    if (message['header']['application'] != APPLICATION)
      throw ('M01: Payload is not a Sirius Sign message string');
    if (message['header']['messageType'] != SiriusSignMessageType.SIGN_NOTIFY)
      throw ('M03: This Sirius Sign Message is not a Cosigner Notifying Message');

    return new SiriusSignCosignerNotifyingMessage(message['header']['appID'],
        message['body']["uploadTxHash"], message['meta']);
  }
}
